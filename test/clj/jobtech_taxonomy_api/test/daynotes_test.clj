(ns jobtech-taxonomy-api.test.daynotes-test
  (:require [clojure.test :as test :refer [deftest is]]
            [jobtech-taxonomy-api.db.daynotes :as daynotes]
            [jobtech-taxonomy-api.test.test-utils :as util]
            [jobtech-taxonomy-api.db.versions :as versions]
            [jobtech-taxonomy-api.db.concepts :as concepts])
  (:import (java.util Date)))

(test/use-fixtures :each util/fixture)

(deftest test-changes-of-created-concept
  (let [_ (versions/create-new-version 0)
        [_ _ concept] (concepts/assert-concept "Stallman" {:type "skill" :definition "Redding" :preferred-label "Reading"})
        [create-event] (daynotes/get-for-concept (:concept/id concept) nil nil)
        time-between-events (Date.)
        _ (is (= "CREATED" (:event-type create-event)))
        _ (is (= concept (:concept create-event)))
        _ (concepts/accumulate-concept "Stallman" {:id (:concept/id concept) :definition "Reading"})
        [_ update-event] (daynotes/get-for-concept (:concept/id concept) nil nil)
        _ (is (= [{:attribute :concept/definition
                   :new-value "Reading"
                   :old-value "Redding"}]
                 (:changes update-event)))]
    (is (= [update-event]
           (daynotes/get-for-concept (:concept/id concept) time-between-events nil)))
    (is (= [create-event]
           (daynotes/get-for-concept (:concept/id concept) nil time-between-events)))))

(deftest test-relations-changes
  (let [_ (versions/create-new-version 0)
        [_ _ reading] (concepts/assert-concept "Stallman" {:type "skill" :definition "Reading" :preferred-label "Reading"})
        [_ _ writing] (concepts/assert-concept "Stallman" {:type "skill" :definition "Writing" :preferred-label "Writing"})
        _ (concepts/assert-relation "Stallman" nil (:concept/id reading) (:concept/id writing)
                                    "related" "They are related!" nil)
        [related-event] (daynotes/get-for-relation (:concept/id reading) nil nil)
        _ (is (= "CREATED" (:event-type related-event)))
        _ (is (= (:concept/id reading) (-> related-event :relation :source :concept/id)))
        _ (is (= (:concept/id writing) (-> related-event :relation :target :concept/id)))
        time-between-events (Date.)
        _ (concepts/assert-relation "Stallman" nil (:concept/id writing) (:concept/id reading)
                                    "broader" "Writing requires reading and more" nil)
        [_ broader-event] (daynotes/get-for-relation (:concept/id writing) nil nil)]
    (is (= [broader-event]
           (daynotes/get-for-relation (:concept/id writing) time-between-events nil)))
    (is (= [related-event]
           (daynotes/get-for-relation (:concept/id writing) nil time-between-events)))))