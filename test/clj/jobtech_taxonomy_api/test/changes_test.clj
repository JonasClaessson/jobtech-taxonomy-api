(ns jobtech-taxonomy-api.test.changes-test
  (:require [clojure.test :as test]
            [jobtech-taxonomy-api.test.test-utils :as util]
            [jobtech-taxonomy-api.db.events :as events]
            [jobtech-taxonomy-api.db.concepts :as concepts]
            [jobtech-taxonomy-api.db.versions :as versions]
            [jobtech-taxonomy-api.db.core :as core]
            [jobtech-taxonomy-api.db.database-connection :as conn]
            [taxonomy]
            [datomic.client.api :as d]))

(test/use-fixtures :each util/fixture)


;;(use 'kaocha.repl)(run 'jobtech-taxonomy-api.test.changes-test/changes-test-0)


(comment
  0 ta ut nuvarande version
  1 skapa concept
  2 publicera ny version
  3 updatera concept
  4 publicera ny version

  kontrollera att rätt event har skapats,
  1 testa att created eventet skapas i rätt version
  1 testa att ändra preferred label)

(defn call-changes-service [after-version to-version-inclusive]
  (let [[status body] (util/send-request-to-json-service
                       :get "/v1/taxonomy/main/changes"
                       :headers [(util/header-auth-user)]
                       :query-params [{:key "after-version", :val after-version}
                                      {:key "to-version-inclusive" :val to-version-inclusive}])]
    [status body]))

(defn create-version-0 []
  (when (empty? (versions/get-all-versions))
    (d/transact (conn/get-conn)
                {:tx-data [{:taxonomy-version/id 0
                            :taxonomy-version/tx "datomic.tx"}]})))

(defn create-concept-and-version []
  (let [_ (create-version-0)
        version 0
        next-version 1
        concept-pl (str (gensym "cykla-"))
        [result timestamp new-concept] (concepts/assert-concept "Stallman" {:type "skill"
                                                                            :definition concept-pl
                                                                            :preferred-label concept-pl})
        _ (versions/create-new-version next-version)
        [statud body] (call-changes-service version next-version)
        created-event (first body)]
    [version next-version created-event new-concept]))

(defn call-private-changes-service [after-version]
  (let [[status body] (util/send-request-to-json-service
                       :get "/v1/taxonomy/private/changes"
                       :headers [(util/header-auth-admin)]
                       :query-params [{:key "after-version", :val after-version}])]
    [status body]))

(defn create-concept []
  (let [_ (create-version-0)
        version 0
        concept-pl (str (gensym "cykla-"))
        [result timestamp new-concept] (concepts/assert-concept "Stallman" {:type "skill"
                                                                            :definition concept-pl
                                                                            :preferred-label concept-pl})
        [statud body] (call-private-changes-service version)
        created-event (first body)]
    [version created-event new-concept]))

(test/deftest ^:integration-changes-test-0 test-changes-unpublished
  (let [[version created-event new-concept] (create-concept)
        concept-from-event (:taxonomy/changed-concept created-event)]
    (test/is (= (:concept/preferred-label new-concept) (:taxonomy/preferred-label concept-from-event)))))

(test/deftest ^:integration-changes-test-0 test-changes-create-concept
  (test/testing "Test create concept and read changes created event"
    (let [[version next-version created-event new-concept] (create-concept-and-version)
          concept-from-event (:taxonomy/changed-concept created-event)]

      (test/is (= (:concept/preferred-label new-concept) (:taxonomy/preferred-label concept-from-event)))
      (test/is (= (:concept/id new-concept) (:taxonomy/id concept-from-event)))
      (test/is (= (:concept/type new-concept) (:taxonomy/type concept-from-event)))
      (test/is (= next-version (:taxonomy/version created-event)))
      (test/is (= "CREATED" (:taxonomy/event-type created-event))))))

(defn update-concept-and-publish-version []
  (let [[version next-version created-event new-concept]  (create-concept-and-version)
        next-next-version (inc next-version)
        new-concept-pl (str (gensym "hoppa-"))
        {:keys [time concept]} (concepts/accumulate-concept "Stallman" {:id (:concept/id new-concept)
                                                                        :type "skill"
                                                                        :preferred-label new-concept-pl})
        _ (versions/create-new-version next-next-version)
        [status body] (call-changes-service next-version next-next-version)
        updated-event (first body)]
    [next-version next-next-version updated-event concept]))

(test/deftest ^:integration-changes-test-1 test-changes-update-concept
  (test/testing "Test update concept and read changes update event"
    (let [[version next-version event concept] (update-concept-and-publish-version)
          concept-from-event (:taxonomy/changed-concept event)]

      (test/is (= (:concept/preferred-label concept) (:taxonomy/preferred-label concept-from-event)))
      (test/is (= (:concept/id concept) (:taxonomy/id concept-from-event)))
      (test/is (= (:concept/type concept) (:taxonomy/type concept-from-event)))
      (test/is (= next-version (:taxonomy/version event)))
      (test/is (= "UPDATED" (:taxonomy/event-type event))))))

(defn deprecate-concept-and-publish-version []
  (let [[version next-version created-event new-concept]  (create-concept-and-version)
        next-next-version (inc next-version)
        _ (core/retract-concept "Stallman" (:concept/id new-concept) nil)
        _ (versions/create-new-version next-next-version)
        [status body] (call-changes-service next-version next-next-version)
        event (first body)]
    [next-version next-next-version event new-concept]))

(test/deftest ^:integration-changes-test-2 test-changes-deprecate-concept
  (test/testing "Test update concept and read changes deprecate event"
    (let [[version next-version event concept] (deprecate-concept-and-publish-version)
          concept-from-event (:taxonomy/changed-concept event)]

      (test/is (= (:concept/preferred-label concept) (:taxonomy/preferred-label concept-from-event)))
      (test/is (= (:concept/id concept) (:taxonomy/id concept-from-event)))
      (test/is (= (:concept/type concept) (:taxonomy/type concept-from-event)))
      (test/is (:taxonomy/deprecated concept-from-event))
      (test/is (= next-version (:taxonomy/version event)))
      (test/is (= "DEPRECATED" (:taxonomy/event-type event))))))
