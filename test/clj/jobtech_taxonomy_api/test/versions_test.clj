(ns jobtech-taxonomy-api.test.versions-test
  (:require [clojure.test :as test :refer [deftest is]]
            [datomic.client.api :as d]
            [jobtech-taxonomy-api.test.test-utils :as util]
            [jobtech-taxonomy-api.db.events :as events]
            [jobtech-taxonomy-api.db.concepts :as concept]
            [jobtech-taxonomy-api.db.versions :as versions]
            [jobtech-taxonomy-api.db.database-connection :refer :all]
            [jobtech-taxonomy-api.db.core :as core])
  (:import (java.util Date)))

(test/use-fixtures :each util/fixture)

(deftest create-version-test
  (d/transact (get-conn) {:tx-data [{:taxonomy-version/id 0
                                     :taxonomy-version/tx "datomic.tx"}]})
  (is (= 0 (versions/get-current-version-id (get-db)))))

(deftest incremented-versions-only
  (d/transact (get-conn) {:tx-data [{:taxonomy-version/id 0
                                     :taxonomy-version/tx "datomic.tx"}]})
  (is (= 1 (:version (versions/create-new-version 1))))
  (is (= 2 (:version (versions/create-new-version 2))))
  (is (= ::versions/incorrect-new-version (versions/create-new-version 10))))

(deftest rollbacks-are-not-allowed
  (let [time-before-release (Date.)]
    (d/transact (get-conn) {:tx-data [{:taxonomy-version/id 0
                                       :taxonomy-version/tx "datomic.tx"}]})
    (is (= 1 (:version (versions/create-new-version 1))))
    (is (= ::versions/new-version-before-release (versions/create-new-version 2 time-before-release)))))