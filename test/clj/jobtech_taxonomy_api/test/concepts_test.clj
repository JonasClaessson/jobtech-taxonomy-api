(ns jobtech-taxonomy-api.test.concepts-test
  (:require [clojure.test :as test :refer [deftest is testing]]
            [jobtech-taxonomy-api.test.test-utils :as util]
            [jobtech-taxonomy-api.db.events :as events]
            [jobtech-taxonomy-api.db.core :as core]
            [jobtech-taxonomy-api.db.concepts :as concepts]
            [jobtech-taxonomy-database.nano-id :as nano]))

(test/use-fixtures :each util/fixture)

(deftest ^:integration-concepts-test-0 concepts-test-0
  (testing "test assert concept"
    (concepts/assert-concept "Stallman" {:type "skill" :definition "cyklade" :preferred-label "cykla"})
    (let [[status body] (util/send-request-to-json-service
                         :get "/v1/taxonomy/main/concepts"
                         :headers [(util/header-auth-user)]
                         :query-params [{:key "type", :val "skill"}])
          found-concept (first (concepts/find-concepts {:preferred-label "cykla" :version :next}))]
      (is (= "cykla" (get found-concept :concept/preferred-label))))))

(deftest ^:integration-concepts-test-1 concepts-test-0
  (testing "test concept relation 'related'"
    (let [id-1 (get-in (concepts/assert-concept "Stallman" {:type "skill"
                                                            :definition "pertest0"
                                                            :preferred-label "pertest0"})
                       [2 :concept/id])
          id-2 (get-in (concepts/assert-concept "Stallman" {:type "skill"
                                                            :definition "pertest1"
                                                            :preferred-label "pertest1"})
                       [2 :concept/id])
          [tx rel] (concepts/assert-relation "Stallman" nil id-1 id-2 "related" "desc" 0)
          rels (concepts/find-relations-including-unpublished {:concept-1 id-1 :concept-2 id-2 :type "related" :limit 1})]
      (is (not (nil? rel)))
      (is (> (count rels) 0)))))

(deftest replacement-deprecates-old-concept
  (let [writing-id (get-in (concepts/assert-concept "Stallman" {:type "skill"
                                                                :definition "writing"
                                                                :preferred-label "Writing"})
                           [2 :concept/id])
        texting-id (get-in (concepts/assert-concept "Stallman" {:type "skill"
                                                                :definition "texting"
                                                                :preferred-label "Texting"})
                           [2 :concept/id])
        _ (core/replace-deprecated-concept "Stallman" writing-id texting-id nil)
        deprecated-writing (first (concepts/find-concepts {:id writing-id :version :next}))]
    (is (:concept/deprecated deprecated-writing))
    (is (= texting-id (-> deprecated-writing :concept/replaced-by first :concept/id)))))
