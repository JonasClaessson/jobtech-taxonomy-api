(defproject jobtech-taxonomy-api "0.1.0-SNAPSHOT"

  :description "FIXME: write description"
  :url "http://example.com/FIXME"

  :dependencies [[buddy "2.0.0"]
                 [ch.qos.logback/logback-classic "1.2.3"]
                 [com.walmartlabs/lacinia "0.36.0"]
                 [superlifter "0.1.1"]
                 [cheshire "5.8.1"]
                 [clojure.java-time "0.3.2"]
                 [cprop "0.1.14"]
                 [expound "0.7.2"]
                 [funcool/struct "1.4.0"]
                 [luminus-immutant "0.2.5"]
                 [com.datomic/client-cloud "0.8.81"]
                 [luminus-transit "0.1.1"]
                 [luminus/ring-ttl-session "0.3.3"]
                 [markdown-clj "1.10.0"]
                 [metosin/muuntaja "0.6.4"]
                 [metosin/reitit "0.3.9"]
                 [metosin/ring-http-response "0.9.1"]
                 [mount "0.1.16"]
                 [nrepl "0.6.0"]
                 [io.replikativ/konserve "0.5.1"]
                 [clj-http "3.10.0"]

                 [org.clojure/clojure "1.10.1"]
                 [org.clojure/tools.cli "0.4.2"]
                 [org.clojure/tools.logging "0.5.0"]
                 [org.clojure/core.async "0.7.559"]
                 [org.webjars.npm/bulma "0.7.5"]
                 [org.webjars.npm/material-icons "0.3.0"]
                 [org.webjars/webjars-locator "0.36"]
                 [ring-webjars "0.2.0"]
                 [ring/ring-core "1.7.1"]
                 [ring/ring-defaults "0.3.2"]
                 [selmer "1.12.14"]
                 [metosin/ring-swagger "0.26.2"]
                 [jobtech-taxonomy-database "0.1.0-SNAPSHOT"]
                 [environ/environ.core "0.3.1"]
                 [metosin/ring-swagger-ui "3.25.0"]]

  :min-lein-version "2.0.0"

  :source-paths ["src/clj"]
  :test-paths ["test/clj"]
  :resource-paths ["resources"]
  :target-path "target/%s/"

  :repositories [["snapshots" {:url "https://repo.clojars.org"
                               :username "batfish"
                               :password :env}]]

  :main ^:skip-aot jobtech-taxonomy-api.core
  :cljfmt {}

  :plugins [[lein-cljfmt "0.6.6"]]

  :profiles
  {:kaocha [:project/kaocha]

   :uberjar {:omit-source true
             :aot :all
             :uberjar-name "jobtech-taxonomy-api.jar"
             :source-paths ["env/prod/clj"]
             :resource-paths ["env/prod/resources"]}

   :dev           [:project/dev :profiles/dev]
   :local         [:project/local :profiles/local]
   :test          [:project/test :profiles/test]

   :project/kaocha {:dependencies [[lambdaisland/kaocha "0.0-418"]]
                    ;; You can only comment in one resource-path:
                    ;; :resource-paths ["env/dev/resources"] ; comment in for local use
                    :resource-paths ["env/integration-test/resources"]} ; comment in for Jenkins
   :project/dev  {:jvm-opts ["-Dconf=dev-config.edn"] ; FIXME: the file referred here does not exist
                  :dependencies [[expound "0.7.2"]
                                 [lambdaisland/kaocha "0.0-418"]
                                 [pjstadig/humane-test-output "0.9.0"]
                                 [prone "1.6.1"]
                                 [org.clojure/test.check "1.0.0"]
                                 [ring/ring-devel "1.7.1"]
                                 [ring/ring-mock "0.4.0"]]
                  :source-paths ["env/dev/clj"]
                  :resource-paths ["env/dev/resources"]
                  :repl-options {:init-ns user}
                  :injections [(require 'pjstadig.humane-test-output)
                               (pjstadig.humane-test-output/activate!)]}
   :project/local {:dependencies [[expound "0.7.2"]
                                  [pjstadig/humane-test-output "0.9.0"]
                                  [prone "1.6.1"]
                                  [ring/ring-devel "1.7.1"]
                                  [ring/ring-mock "0.3.2"]]
                   :source-paths ["env/local/clj"]
                   :resource-paths ["env/local/resources"]
                   :injections [(require 'pjstadig.humane-test-output)
                                (pjstadig.humane-test-output/activate!)]
                   :repl-options {:init-ns user}}
   :project/test {:jvm-opts ["-Dconf=test-config.edn"]
                  :resource-paths ["env/test/resources"]}
   :profiles/dev {}
   :profiles/local {}
   :profiles/test  {}}
  :aliases {"kaocha" ["with-profile" "+kaocha" "run" "-m" "kaocha.runner"]})
