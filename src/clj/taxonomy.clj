(ns taxonomy
  (:require [clojure.spec.alpha :as sp]
            [clojure.string :as str]
            [spec-tools.core :as st]
            [spec-tools.data-spec :as ds]
            [clojure.spec.alpha :as s]))

(def taxonomy-namespace "taxonomy")

(defn map->nsmap
  "Apply our nice namespace to the supplied structure."
  [m]
  (reduce-kv (fn [acc k v]
               (let [new-kw (if (keyword? k)
                              (keyword (str taxonomy-namespace) (name k))
                              k)
                     new-v (if (= (type v) clojure.lang.PersistentArrayMap)
                             (map->nsmap v)
                             (if (or (= (type v) clojure.lang.PersistentList)
                                     (= (type v) clojure.lang.PersistentVector)
                                     (= (type v) clojure.lang.LazySeq))
                               (map map->nsmap v)
                               v))]
                 (assoc acc new-kw new-v)))
             {} m))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;; Output response types

;; General fundamentals


(defmacro par "Use this to make parameter declarations somewhat tidier." [type desc & kvs]
  `(st/spec ~(merge {:spec type :description desc} (apply hash-map kvs))))

(sp/def ::id (st/spec string?))
(sp/def ::type (st/spec string?))
(sp/def ::deprecated (st/spec boolean?))
(sp/def ::preferred-label (st/spec string?))
(sp/def ::definition (st/spec string?))
(sp/def ::event-type (st/spec string?))
(sp/def ::version (st/spec int?))
(sp/def ::quality-level
  (par (sp/and int? #{1 2 3}) ;; `int?` is needed to trigger reitit's coercion mechanism
       "Concept quality level"
       :swagger/type :integer
       :swagger/enum [1 2 3]))
(sp/def ::deprecated-legacy-id
  (st/spec string? {:description (str "Internal ID that exists only for concepts imported from the old taxonomy database. "
                                      "These IDs are unique per type.")}))

(sp/def ::broader (st/spec int?))
(sp/def ::narrower (st/spec int?))
(sp/def ::related (st/spec int?))
(sp/def ::substitutability-to (st/spec int?))
(sp/def ::substitutability-from (st/spec int?))
(sp/def ::concept-1 (st/spec string?))
(sp/def ::concept-2 (st/spec string?))

(sp/def ::relations
  (st/spec
   {:name ::relations
    :spec (sp/keys :req [::broader ::narrower ::related ::substitutability-to ::substitutability-from])}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ssyk-2012

(sp/def ::ssyk-code-2012 (st/spec string?))

(sp/def ::concept-ssyk-shallow
  (st/spec
   {:name ::concept-ssyk-shallow
    :spec (sp/keys :req [::id ::type ::ssyk-code-2012]
                   :opt [::definition ::deprecated ::preferred-label ::relations])}))

(sp/def ::replaced-by-ssyk
  (ds/spec
   {:name ::replaced-by-ssyk :spec (sp/coll-of ::concept-ssyk-shallow)}))

(sp/def ::concept-ssyk
  (ds/spec
   {:name ::concept-ssyk
    :spec (sp/keys :req [::id ::type ::ssyk-code-2012 ::preferred-label]
                   :opt [::definition ::deprecated ::replaced-by-ssyk ::relations ::deprecated-legacy-id
                         ::quality-level])}))

(sp/def ::concepts-ssyk
  (ds/spec
   {:name ::concepts-ssyk
    :spec (sp/coll-of ::concept-ssyk)}))

;; municipality

(sp/def ::lau-2-code-2015 (st/spec string?))

(sp/def ::concept-municipality-shallow
  (st/spec
   {:name ::concept-municipality-shallow
    :spec (sp/keys :req [::id ::type ::lau-2-code-2015]
                   :opt [::definition ::deprecated ::preferred-label ::relations])}))

(sp/def ::replaced-by-municipality
  (ds/spec
   {:name ::replaced-by-municipality
    :spec (sp/coll-of ::concept-municipality-shallow)}))

(sp/def ::concept-municipality
  (ds/spec
   {:name ::concept-municipality
    :spec (sp/keys :req [::id ::type ::lau-2-code-2015 ::preferred-label]
                   :opt [::definition ::deprecated ::replaced-by-municipality ::relations ::deprecated-legacy-id
                         ::quality-level])}))

(sp/def ::concepts-municipality
  (ds/spec
   {:name ::concepts-municipality
    :spec (sp/coll-of ::concept-municipality)}))


;; eures-code


(sp/def ::eures-code-2014 (st/spec string?))

(sp/def ::concept-employment-duration-shallow
  (st/spec
   {:name ::concept-employment-duration-shallow
    :spec (sp/keys :req [::id ::type ::eures-code-2014]
                   :opt [::definition ::deprecated ::preferred-label ::relations])}))

(sp/def ::replaced-by-employment-duration
  (ds/spec
   {:name ::replaced-by-employment-duration
    :spec (sp/coll-of ::concept-employment-duration-shallow)}))

(sp/def ::concept-employment-duration
  (ds/spec
   {:name ::concept-employment-duration
    :spec (sp/keys :req [::id ::type ::eures-code-2014 ::preferred-label]
                   :opt [::definition ::deprecated ::replaced-by-employment-duration ::relations ::deprecated-legacy-id
                         ::quality-level])}))

(sp/def ::concepts-employment-duration
  (ds/spec
   {:name ::concepts-employment-duration
    :spec (sp/coll-of ::concept-employment-duration)}))

(def concepts-spec-eures-code ::concepts-employment-duration)


;; nuts-level-3


(sp/def ::nuts-level-3-code-2013 (st/spec string?))
(sp/def ::national-nuts-level-3-code-2019 (st/spec string?))

(sp/def ::concept-region-shallow
  (st/spec
   {:name ::concept-region-shallow
    :spec (sp/keys :req [::id ::type]
                   :opt [::definition ::deprecated ::preferred-label ::relations ::national-nuts-level-3-code-2019
                         ::nuts-level-3-code-2013])}))

(sp/def ::replaced-by-region
  (ds/spec
   {:name ::replaced-by-region
    :spec (sp/coll-of ::concept-region-shallow)}))

(sp/def ::concept-region
  (ds/spec
   {:name ::concept-region
    :spec (sp/keys :req [::id ::type ::preferred-label]
                   :opt [::definition ::deprecated ::replaced-by-region ::relations
                         ::national-nuts-level-3-code-2019 ::nuts-level-3-code-2013 ::deprecated-legacy-id
                         ::quality-level])}))

(sp/def ::concepts-region
  (ds/spec
   {:name ::concepts-region
    :spec (sp/coll-of ::concept-region)}))

;; country

(sp/def ::iso-3166-1-alpha-3-2013 (st/spec string?))
(sp/def ::iso-3166-1-alpha-2-2013 (st/spec string?))

(sp/def ::concept-country-shallow
  (st/spec
   {:name ::concept-country-shallow
    :spec (sp/keys :req [::id ::iso-3166-1-alpha-3-2013 ::iso-3166-1-alpha-2-2013 ::type]
                   :opt [::definition ::deprecated ::preferred-label ::relations])}))

(sp/def ::replaced-by-country
  (ds/spec
   {:name ::replaced-by-country
    :spec (sp/coll-of ::concept-country-shallow)}))

(sp/def ::concept-country
  (ds/spec
   {:name ::concept-country
    :spec (sp/keys :req [::id ::type ::iso-3166-1-alpha-3-2013 ::iso-3166-1-alpha-2-2013 ::preferred-label]
                   :opt [::definition ::deprecated ::replaced-by-country ::relations ::deprecated-legacy-id
                         ::quality-level])}))

(sp/def ::concepts-country
  (ds/spec
   {:name ::concepts-country
    :spec (sp/coll-of ::concept-country)}))

;; id ;; TODO: make completions

(sp/def ::concept-id-shallow
  (st/spec
   {:name ::concept-id-shallow
    :spec (sp/keys :req [::id ::type]
                   :opt [::definition ::deprecated ::preferred-label ::relations])}))

(sp/def ::replaced-by-id
  (ds/spec
   {:name ::replaced-by-id
    :spec (sp/coll-of ::concept-id-shallow)}))

(sp/def ::concept-id
  (ds/spec
   {:name ::concept-id
    :spec (sp/keys :req [::id ::type ::preferred-label]
                   :opt [::definition ::deprecated ::replaced-by-id ::relations ::quality-level])}))

(sp/def ::concepts-id
  (ds/spec
   {:name ::concepts-id
    :spec (sp/coll-of ::concept-id)}))

;; isco-08

(sp/def ::isco-code-08 (st/spec string?))

(sp/def ::concept-isco-shallow
  (st/spec
   {:name ::concept-isco-shallow
    :spec (sp/keys :req [::id ::isco-code-08 ::type]
                   :opt [::definition ::deprecated ::preferred-label ::relations])}))

(sp/def ::replaced-by-isco
  (ds/spec
   {:name ::replaced-by-isco
    :spec (sp/coll-of ::concept-isco-shallow)}))

(sp/def ::concept-isco
  (ds/spec
   {:name ::concept-isco
    :spec (sp/keys :req [::id ::type ::isco-code-08 ::preferred-label]
                   :opt [::definition ::deprecated ::replaced-by-isco ::relations ::deprecated-legacy-id
                         ::quality-level])}))

(sp/def ::concepts-isco
  (ds/spec
   {:name ::concepts-isco
    :spec (sp/coll-of ::concept-isco)}))

;; education-field-code-2020

(sp/def ::sun-education-field-code-2020 (st/spec string?))
(sp/def ::sun-education-field-code-2000 (st/spec string?))

(sp/def ::concept-sun-education-field-shallow
  (st/spec
   {:name ::concept-sun-education-field-shallow
    :spec (sp/keys :req [::id  ::type]
                   :opt [::definition ::sun-education-field-code-2020 ::sun-education-field-code-2000 ::deprecated ::preferred-label ::relations])}))

(sp/def ::replaced-by-sun-education-field
  (ds/spec
   {:name ::replaced-by-sun-education-field
    :spec (sp/coll-of ::concept-sun-education-field-shallow)}))

(sp/def ::concept-sun-education-field
  (ds/spec
   {:name ::concept-sun-education-field
    :spec (sp/keys :req [::id ::type  ::preferred-label]
                   :opt [::definition ::sun-education-field-code-2020 ::sun-education-field-code-2000 ::deprecated ::replaced-by-sun-education-field ::relations ::deprecated-legacy-id
                         ::quality-level])}))

(sp/def ::concepts-sun-education-field
  (ds/spec
   {:name ::concepts-sun-education-field
    :spec (sp/coll-of ::concept-sun-education-field)}))

;; sun-education-level-code-2020

(sp/def ::sun-education-level-code-2020 (st/spec string?))
(sp/def ::sun-education-level-code-2000 (st/spec string?))

(sp/def ::concept-sun-education-level-shallow
  (st/spec
   {:name ::concept-sun-education-level-shallow
    :spec (sp/keys :req [::id  ::type]
                   :opt [::sun-education-level-code-2020 ::sun-education-level-code-2000 ::definition ::deprecated ::preferred-label ::relations])}))

(sp/def ::replaced-by-sun-education-level
  (ds/spec
   {:name ::replaced-by-sun-education-level
    :spec (sp/coll-of ::concept-sun-education-level-shallow)}))

(sp/def ::concept-sun-education-level
  (ds/spec
   {:name ::concept-sun-education-level
    :spec (sp/keys :req [::id ::type  ::preferred-label]
                   :opt [::definition ::sun-education-level-code-2020 ::sun-education-level-code-2000 ::deprecated ::replaced-by-sun-education-level ::relations ::deprecated-legacy-id
                         ::quality-level])}))

(sp/def ::concepts-sun-education-level
  (ds/spec
   {:name ::concepts-sun-education-level
    :spec (sp/coll-of ::concept-sun-education-level)}))

;; sni-level-code

(sp/def ::sni-level-code-2007 (st/spec string?))

(sp/def ::concept-sni-level-shallow
  (st/spec
   {:name ::concept-sni-level-shallow
    :spec (sp/keys :req [::id ::sni-level-code-2007 ::type]
                   :opt [::definition ::deprecated ::preferred-label ::relations])}))

(sp/def ::replaced-by-sni-level
  (ds/spec
   {:name ::replaced-by-sni-level
    :spec (sp/coll-of ::concept-sni-level-shallow)}))

(sp/def ::concept-sni-level
  (ds/spec
   {:name ::concept-sni-level
    :spec (sp/keys :req [::id ::type ::sni-level-code-2007 ::preferred-label]
                   :opt [::definition ::deprecated ::replaced-by-sni-level ::relations ::deprecated-legacy-id
                         ::quality-level])}))

(sp/def ::concepts-sni-level
  (ds/spec
   {:name ::concepts-sni-level
    :spec (sp/coll-of ::concept-sni-level)}))

;; language

(sp/def ::iso-639-3-alpha-2-2007 (st/spec string?))
(sp/def ::iso-639-3-alpha-3-2007 (st/spec string?))

(sp/def ::concept-language-shallow
  (st/spec
   {:name ::concept-language-shallow
    :spec (sp/keys :req [::id ::iso-639-3-alpha-3-2007 ::type ::preferred-label]
                   :opt [::definition ::deprecated ::relations ::iso-639-3-alpha-2-2007])}))

(sp/def ::replaced-by-language
  (ds/spec
   {:name ::replaced-by-language
    :spec (sp/coll-of ::concept-language-shallow)}))

(sp/def ::concept-language
  (ds/spec
   {:name ::concept-language
    :spec (sp/keys :req [::id ::type ::iso-639-3-alpha-3-2007 ::preferred-label]
                   :opt [::definition ::deprecated ::replaced-by-language ::relations ::iso-639-3-alpha-2-2007
                         ::deprecated-legacy-id ::quality-level])}))

(sp/def ::concepts-language
  (ds/spec
   {:name ::concepts-language
    :spec (sp/coll-of ::concept-language)}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; /versions

(sp/def ::version-object
  (ds/spec
   {:name ::version-object
    :spec {::version int?
           ::timestamp inst?}}))

(sp/def ::versions
  (ds/spec
   {:name ::versions
    :spec (sp/coll-of ::version-object)}))

(def versions-spec ::versions)

;; /changes
(sp/def ::changed-concept
  (st/spec
   {:name ::changed-concept
    :spec (sp/keys :req [::id ::type]
                   :opt [::definition ::deprecated ::preferred-label])}))

(sp/def ::event
  (ds/spec
   {:name ::event
    :spec (sp/keys :req [::event-type
                         ::changed-concept]
                   :opt [::version])}))

(sp/def ::events
  (ds/spec
   {:name ::events
    :spec (sp/coll-of ::event)}))

(def events-spec ::events)

;; /concepts

(sp/def ::concept-shallow
  (ds/spec
   {:name ::concept-shallow
    :spec (sp/keys :req [::id ::type ::preferred-label]
                   :opt [::definition ::deprecated])}))

(sp/def ::concepts-shallow
  (ds/spec
   {:name ::concepts-shallow
    :spec (sp/coll-of ::concept-shallow)}))

(sp/def ::replaced-by
  (ds/spec
   {:name ::replaced-by
    :spec (sp/coll-of ::concept-shallow)}))

(sp/def ::concept
  (ds/spec
   {:name ::concept
    :spec (sp/keys :req [::id ::type ::preferred-label]
                   :opt [::definition ::deprecated ::replaced-by ::relations ::deprecated-legacy-id ::quality-level])}))

(sp/def ::concepts
  (ds/spec
   {:name ::concepts
    :spec (sp/coll-of ::concept)}))

(def concepts-spec ::concepts)

;; /autocomplete


(def autocomplete-spec ::concepts-shallow)

;; /replaced-by-changes


(sp/def ::replaced-by-change
  (ds/spec
   {:name ::replaced-by-change
    :spec (sp/keys :req [::version ::concept])}))

(sp/def ::replaced-by-changes
  (ds/spec
   {:name ::replaced-by-changes
    :spec (sp/coll-of ::replaced-by-change)}))

(def replaced-by-changes-spec ::replaced-by-changes)

;; /concept/types

(sp/def ::concept-types
  (ds/spec
   {:name ::concept-types
    :spec (sp/coll-of string?)}))

(def concept-types-spec ::concept-types)

;; /relation/types

(sp/def ::relation-types
  (ds/spec
   {:name ::relation-types
    :spec (sp/coll-of string?)}))

(def relation-types-spec ::relation-types)

;; /parse-text

(def parse-text-spec ::concepts-shallow)




;; Error message


(sp/def ::err (ds/spec {:name "error"
                        :spec {::error (st/spec string?)}}))
(def error-spec ::err)

(sp/def ::ok (ds/spec {:name "ok"
                       :spec {::message (st/spec string?)}}))
(def ok-spec ::ok)

(sp/def ::msg (ds/spec {:name "msg"
                        :spec {::message (st/spec string?)}}))
(def msg-spec ::msg)

(sp/def ::ok-concept (ds/spec {:name "ok"
                               :spec {::time inst?
                                      ::concept ::concept}}))
(def ok-concept-spec ::ok-concept)

(sp/def ::unauthorized (ds/spec {:name "unauthorized"
                                 :spec {::error (st/spec string?)}}))
(def unauthorized-spec ::unauthorized)

(sp/def ::source (st/spec string?))
(sp/def ::target (st/spec string?))
(sp/def ::relation-type (st/spec string?))
(sp/def ::substitutability-percentage (st/spec int?))

(sp/def ::edge (ds/spec {:name "edge"
                         :spec (sp/keys :req [::source
                                              ::target
                                              ::relation-type]
                                        :opt [::substitutability-percentage])}))

(sp/def ::edges (ds/spec {:name "edges"
                          :spec (sp/coll-of ::edge)}))

(sp/def ::nodes (ds/spec {:name "nodes"
                          :spec (sp/coll-of ::concept-shallow)}))

(sp/def ::graph (ds/spec {:name "graph"
                          :spec (sp/keys :opt [::edges ::nodes])}))

(def graph-spec ::graph)

(declare koncept-spec)

(sp/def :concept/id (st/spec string?))
(sp/def :concept/type (st/spec string?))
(sp/def :concept/deprecated (st/spec boolean?))
(sp/def :concept/preferred-label (st/spec string?))
(sp/def :concept/definition (st/spec string?))

(def koncept
  {(ds/req :id) :concept/id
   (ds/req :type) :concept/type
   (ds/req :preferred-label) :concept/preferred-label
   (ds/opt :definition) :concept/definition
   (ds/opt :deprecated) :concept/deprecated
   (ds/opt :replaced-by) koncept-spec})

(def koncept-spec
  (ds/spec
   {:name :taxonomy/Koncept
    :spec koncept}))

;; TODO add language

;; TODO FIX implicit driving licence spec!!


;; driving-licence


(sp/def ::driving-licence-code-2013 (st/spec string?))

(sp/def ::concept-driving-licence-shallow
  (st/spec
   {:name ::concept-driving-licence-shallow
    :spec (sp/keys :req [::id ::type ::driving-licence-code-2013]
                   :opt [::definition ::deprecated ::preferred-label ::relations])}))

(sp/def ::replaced-by-driving-licence
  (ds/spec
   {:name ::replaced-by-driving-licence
    :spec (sp/coll-of ::concept-driving-licence-shallow)}))

(sp/def ::concept-driving-licence
  (ds/spec
   {:name ::concept-driving-licence
    :spec (sp/keys :req [::id ::type ::driving-licence-code-2013 ::preferred-label]
                   :opt [::definition ::deprecated ::replaced-by-driving-licence ::relations ::deprecated-legacy-id])}))

(sp/def ::concepts-driving-licence
  (ds/spec
   {:name ::concepts-driving-licence
    :spec (sp/coll-of ::concept-driving-licence)}))

;; Legacy

(sp/def ::legacy-concept
  (ds/spec {:name ::legacy-concept
            :spec (sp/keys :req [::id ::type ::preferred-label ::deprecated-legacy-id]
                           :opt [::definition ::ssyk-code-2012 ::lau-2-code-2015 ::national-nuts-level-3-code-2019])}))

(def legacy-concept ::legacy-concept)

;; legacy mapping object

(sp/def ::ssyk-id (st/spec string?))
(sp/def ::ssyk-deprecated-legacy-id (st/spec string?))
(sp/def ::occupation-field-id (st/spec string?))
(sp/def ::occupation-field-deprecated-legacy-id (st/spec string?))

(sp/def ::legacy-mapping-concept
  (ds/spec {:name ::legacy-mapping-concept
            :spec (sp/keys :req [::id ::type ::preferred-label ::deprecated-legacy-id]
                           :opt [::definition
                                 ::ssyk-id
                                 ::ssyk-code-2012
                                 ::ssyk-deprecated-legacy-id
                                 ::occupation-field-id
                                 ::occupation-field-deprecated-legacy-id
                                 ::quality-level])}))

(sp/def  ::legacy-mapping-concepts
  (ds/spec
   {:name ::legacy-mapping-concepts
    :spec (sp/coll-of ::legacy-mapping-concept)}))

(def legacy-mapping-concepts ::legacy-mapping-concepts)

(def detailed-endpoint-configs
  (sort-by :endpoint-name
           [{:endpoint-name "driving-licence"
             :default-type "driving-licence"
             :extra-attributes [{:name :driving-licence-code-2013
                                 :doc "Driving licence code"
                                 :where-field :concept.external-standard/driving-licence-code-2013}]
             :pull [:concept.external-standard/driving-licence-code-2013
                    {:concept.external-standard/implicit-driving-licences
                     [:concept/id
                      :concept.external-standard/driving-licence-code-2013]}]}

            {:endpoint-name "ssyk"
             :default-type "ssyk-level-1 ssyk-level-2 ssyk-level-3 ssyk-level-4"
             :extra-attributes [{:name :ssyk-code-2012
                                 :doc "SSYK 2012"
                                 :where-field :concept.external-standard/ssyk-code-2012}
                                {:name :type
                                 :doc "SSYK level types (space-separated): ssyk-level-1, ssyk-level-2, ssyk-level-3 or ssyk-level-4"}]
             :pull [:concept.external-standard/ssyk-code-2012]}

            {:endpoint-name "municipality"
             :default-type "municipality"
             :extra-attributes [{:name :lau-2-code-2015
                                 :doc "Swedish municipality code"
                                 :where-field :concept.external-standard/lau-2-code-2015}]
             :pull [:concept.external-standard/lau-2-code-2015]}

            {:endpoint-name "region"
             :default-type "region"
             :extra-attributes [{:name :nuts-level-3-code-2013
                                 :doc "Nuts level 3 code"
                                 :where-field :concept.external-standard/nuts-level-3-code-2013}
                                {:name :national-nuts-level-3-code-2019
                                 :doc "Swedish län code"
                                 :where-field :concept.external-standard/national-nuts-level-3-code-2019}]
             :pull [:concept.external-standard/nuts-level-3-code-2013
                    :concept.external-standard/national-nuts-level-3-code-2019]}

            {:endpoint-name "country"
             :default-type "country"
             :extra-attributes [{:name :iso-3166-1-alpha-2-2013
                                 :doc "Country code 2 letter"
                                 :where-field :concept.external-standard/iso-3166-1-alpha-2-2013}
                                {:name :iso-3166-1-alpha-3-2013
                                 :doc "Country code 3 letter"
                                 :where-field :concept.external-standard/iso-3166-1-alpha-3-2013}]
             :pull [:concept.external-standard/iso-3166-1-alpha-2-2013
                    :concept.external-standard/iso-3166-1-alpha-3-2013]}

            {:endpoint-name "isco"
             :default-type "isco-level-4"
             :extra-attributes [{:name :isco-code-08
                                 :doc "ISCO code 2008"
                                 :where-field :concept.external-standard/isco-code-08}]
             :pull [:concept.external-standard/isco-code-08]}

            {:endpoint-name "sun-education-field"
             :default-type "sun-education-field-1 sun-education-field-2 sun-education-field-3 sun-education-field-4"
             :extra-attributes [{:name :sun-education-field-code-2020
                                 :doc "SUN education field 2020 code, either 1, 2 or 3 digits and a letter"
                                 :where-field :concept.external-standard/sun-education-field-code-2020}

                                {:name :sun-education-field-code-2000
                                 :doc "DEPRECATED! Old SUN 2000 education field code, either 1, 2 or 3 digits and a letter"
                                 :where-field :concept.external-standard/sun-education-field-code-2000}

                                {:name :type
                                 :doc "SUN education field types (space-separated): sun-education-field-1, sun-education-field-2, sun-education-field-3 or sun-education-field-4"}]
             :pull [:concept.external-standard/sun-education-field-code-2020
                    :concept.external-standard/sun-education-field-code-2000]}

            {:endpoint-name "sun-education-level"
             :default-type "sun-education-level-1 sun-education-level-2 sun-education-level-3"
             :extra-attributes [{:name :sun-education-level-code-2020
                                 :doc "SUN education level code, either 1, 2 or 3 digits"
                                 :where-field :concept.external-standard/sun-education-level-code-2020}

                                {:name :sun-education-level-code-2000
                                 :doc "DEPRECATED! SUN 2000 education level code, either 1, 2 or 3 digits"
                                 :where-field :concept.external-standard/sun-education-level-code-2000}

                                {:name :type
                                 :doc "SUN education level types (space-separated): sun-education-level-1, sun-education-level-2 or sun-education-level-3"}]
             :pull [:concept.external-standard/sun-education-level-code-2020
                    :concept.external-standard/sun-education-level-code-2000]}

            {:endpoint-name "sni-level"
             :default-type "sni-level-1 sni-level-2"
             :extra-attributes [{:name :sni-level-code-2007
                                 :doc "SNI 2007 level code"
                                 :where-field :concept.external-standard/sni-level-code-2007}
                                {:name :type
                                 :doc "SNI level type: sni-level-1 or sni-level-2"}]
             :pull [:concept.external-standard/sni-level-code-2007]}

            {:endpoint-name "language"
             :default-type "language"
             :extra-attributes [{:name :iso-639-3-alpha-2-2007
                                 :doc "2 letter language code"
                                 :where-field :concept.external-standard/iso-639-3-alpha-2-2007}
                                {:name :iso-639-3-alpha-3-2007
                                 :doc "3 letter language code"
                                 :where-field :concept.external-standard/iso-639-3-alpha-3-2007}]
             :pull [:concept.external-standard/iso-639-3-alpha-2-2007
                    :concept.external-standard/iso-639-3-alpha-3-2007]}

            {:endpoint-name "employment-duration"
             :default-type  "employment-duration"
             :extra-attributes [{:name :eures-code-2014
                                 :doc "Eures code"
                                 :where-field :concept.external-standard/eures-code-2014}]

             :pull [:concept.external-standard/eures-code-2014]}]))

;; TODO add language

(def version-param
  (par
   (s/or :int int?
         ;; `keyword?` is needed only to trigger reitit's coercion mechanism
         :ref (s/and keyword? #{:latest :next}))
   "Taxonomy version, either number that indicates the version, \"latest\" for latest release, or \"next\" for unpublished changes (requires admin rights)"
   :swagger/type {:oneOf [{:type :integer}
                          {:type :string
                           :enum [:latest :next]}]}))

(def detailed-endpoint-query-base
  {(ds/opt :id) (par string? "ID of concept")
   (ds/opt :preferred-label) (par string? "Textual name of concept")
   (ds/opt :deprecated) (par boolean? "Restrict to deprecation state")
   (ds/opt :relation) (par #{"broader" "narrower" "related" "substitutability-to" "substitutability-from"} "Relation type")
   (ds/opt :related-ids) (par string? "OR-restrict to these relation IDs (white space separated list)")
   (ds/opt :include-legacy-information) (taxonomy/par boolean? "This parameter will be removed. Include information related to Arbetsförmedlingen's old internal taxonomy"
                                                      :swagger/deprecated true)

   ;;   (ds/opt :code) (par string? name) ;; TODO Create one for each attribute
   (ds/opt :offset) (par int? "Return list offset (from 0)")
   (ds/opt :limit) (par int? "Return list limit")
   (ds/opt :version) version-param})

(defn create-extra-query-spec-field [extra-query-attribute]
  {(ds/opt (:name extra-query-attribute)) (par string? (:doc extra-query-attribute))})

(defn compose-extra-where-attribute [extra-attribute query-params]
  (let [db-field-name (:where-field extra-attribute)
        field-name (:name extra-attribute)
        value (field-name query-params)]
    [db-field-name value]))

(defn compose-extra-where-attributes [query-params extra-attributes]
  (->> extra-attributes
       (filter :where-field)
       (map #(compose-extra-where-attribute % query-params))
       (filter second)))

(defn build-db-function-args [query-params-map endpoint-config]
  (let [query-params (:query (:parameters query-params-map))
        extra-attributes (:extra-attributes endpoint-config)]
    (-> query-params
        (cond-> (:related-ids query-params)
          (update :related-ids str/split #" "))
        (update :type (fnil str/split (:default-type endpoint-config)) #" ")
        (assoc :extra-pull-fields (:pull endpoint-config))
        (assoc :extra-where-attributes (compose-extra-where-attributes query-params extra-attributes)))))

(defn compose-handler-function [endpoint-config logging-function db-function]
  (fn [args]
    (logging-function (str "\n" (:query (:parameters args))))
    {:status 200
     :body (vec (map map->nsmap (db-function (build-db-function-args args endpoint-config))))}))

(defn create-detailed-endpoint [endpoint-config logging-function db-function]
  (let [endpoint-name (:endpoint-name endpoint-config)
        query (->> endpoint-config
                   :extra-attributes
                   (map create-extra-query-spec-field)
                   (into detailed-endpoint-query-base))]
    [(str "/concepts/" endpoint-name)
     {:summary (str "Get " endpoint-name ". Supply at least one search parameter.")
      :parameters {:query query}
      :get {:responses {200 {:body (keyword taxonomy-namespace (str "concepts-" endpoint-name))}
                        500 {:body error-spec}}
            :handler (compose-handler-function endpoint-config logging-function db-function)}}]))
