(ns jobtech-taxonomy-api.db.legacy
  (:require
   [clojure.set :as set]
   [schema.core :as s]
   [datomic.client.api :as d]
   [jobtech-taxonomy-database.nano-id :as nano]
   [jobtech-taxonomy-api.db.database-connection :refer :all]
   [jobtech-taxonomy-api.db.api-util :refer :all]
   [jobtech-taxonomy-api.db.api-util :as api-util]
   [clojure.set :as set]
   [clojure.tools.logging :as log]
   [jobtech-taxonomy-api.routes.parameter-util :as pu]
   [taxonomy :as types]
   [clojure.spec.alpha :as sp]))

(sp/def ::type
  #{"continent"
    "country"
    "driving-licence"
    "employment-duration"
    "employment-type"
    "language"
    "language-level"
    "municipality"
    "occupation-collection"
    "occupation-experience-year"
    "occupation-field"
    "occupation-name"
    "region"
    "skill"
    "skill-headline"
    "sni-level-1"
    "sni-level-2"
    "ssyk-level-4"
    "wage-type"
    "worktime-extent"})

(def concept-pull-pattern [:concept/id
                           :concept/type
                           :concept/preferred-label
                           :concept/definition
                           :concept.external-database.ams-taxonomy-67/id
                           :concept.external-standard/ssyk-code-2012
                           :concept.external-standard/lau-2-code-2015
                           :concept.external-standard/national-nuts-level-3-code-2019])

(def get-occupation-name-with-relations-query
  '[:find ?id ?type ?pl ?df ?on-legacy-id ?ssyk-id ?ssyk-code  ?ssyk-legacy-id ?of-id ?of-legacy-id

    :keys id type preferred-label definition deprecated-legacy-id ssyk-id ssyk-code-2012 ssyk-deprecated-legacy-id occupation-field-id occupation-field-deprecated-legacy-id

    :in $
    :where

    [?on :concept/type "occupation-name"]
    [?on :concept/type ?type]
    [?on :concept/id ?id]
    [?on :concept/preferred-label ?pl]
    [?on :concept/definition ?df]
    [?on :concept.external-database.ams-taxonomy-67/id ?on-legacy-id]

    [?on-r-ssyk :relation/concept-1 ?on]
    [?on-r-ssyk :relation/concept-2 ?ssyk]
    [?on-r-ssyk :relation/type "broader"]

    [?ssyk :concept/id ?ssyk-id]
    [?ssyk :concept.external-standard/ssyk-code-2012 ?ssyk-code]
    [?ssyk :concept.external-database.ams-taxonomy-67/id ?ssyk-legacy-id]

    [?ssyk-r-of :relation/concept-1 ?ssyk]
    [?ssyk-r-of :relation/concept-2 ?of]
    [?ssyk-r-of :relation/type "broader"]

    [?of :concept/id ?of-id]
    [?of :concept/type "occupation-field"]
    [?of :concept.external-database.ams-taxonomy-67/id ?of-legacy-id]])

(defn get-occupation-name-with-relations []
  (d/q {:query get-occupation-name-with-relations-query  :args [(get-db 1)]}))

(def get-concept-id-from-legacy-id-query
  '[:find (pull ?c pull-pattern)
    :in $ pull-pattern ?legacy-id ?type
    :where
    [?c :concept/type ?type]
    [?c :concept.external-database.ams-taxonomy-67/id ?legacy-id]])

(def get-concept-id-from-ssyk-2012-query
  '[:find (pull ?c pull-pattern)
    :in $ pull-pattern ?ssyk-2012
    :where
    [?c :concept/type "ssyk-level-4"]
    [?c  :concept.external-standard/ssyk-code-2012 ?ssyk-2012]])

(def get-concept-id-from-municipality-code-query
  '[:find (pull ?c pull-pattern)
    :in $ pull-pattern ?code
    :where
    [?c :concept/type "municipality"]
    [?c :concept.external-standard/lau-2-code-2015 ?code]])

(def get-concept-id-from-swedish-region-code-query
  '[:find (pull ?c pull-pattern)
    :in $ pull-pattern ?code
    :where
    [?c :concept/type "region"]
    [?c :concept.external-standard/national-nuts-level-3-code-2019 ?code]])

(def get-legacy-concept-from-new-id-query
  '[:find (pull ?c pull-pattern)
    :in $ pull-pattern ?id
    :where
    [?c :concept/id ?id]])

(defn rename-database-keys [data]
  (set/rename-keys  data {:concept.external-database.ams-taxonomy-67/id :concept/deprecated-legacy-id}))

(defn get-concept-id-from-legacy-id [legacy-id type]
  (rename-database-keys (ffirst (d/q get-concept-id-from-legacy-id-query (get-db 1) concept-pull-pattern legacy-id type))))

(defn get-concept-id-from-ssyk-2012 [ssyk-2012]
  (rename-database-keys (ffirst (d/q get-concept-id-from-ssyk-2012-query (get-db 1) concept-pull-pattern ssyk-2012))))

(defn get-concept-id-from-municipality-code [code]
  (rename-database-keys (ffirst (d/q get-concept-id-from-municipality-code-query  (get-db 1) concept-pull-pattern code))))

(defn get-concept-id-from-swedish-region-code [code]
  (rename-database-keys (ffirst (d/q get-concept-id-from-swedish-region-code-query (get-db 1) concept-pull-pattern  code))))

(defn get-concept-id-from-matching-component-id [matching-component-id type]
  (case type
    "ssyk-level-4" (get-concept-id-from-ssyk-2012 matching-component-id)
    "occupation-group" (get-concept-id-from-ssyk-2012 matching-component-id)
    "municipality" (get-concept-id-from-municipality-code matching-component-id)
    "region" (get-concept-id-from-swedish-region-code matching-component-id)
    (get-concept-id-from-legacy-id matching-component-id type)))

(defn get-legacy-concept-from-new-id [id]
  (rename-database-keys (ffirst (d/q get-legacy-concept-from-new-id-query (get-db 1) concept-pull-pattern id))))

(def responses
  {200 {:body types/legacy-concept}
   404 {:body types/error-spec}
   500 {:body types/error-spec}})

(def concepts-types
  (taxonomy/par #{"continent",
                  "country",
                  "driving-licence",
                  "employment-duration",
                  "employment-type",
                  "language",
                  "language-level",
                  "municipality",
                  "occupation-collection",
                  "occupation-field",
                  "occupation-name",
                  "region",
                  "skill",
                  "skill-headline",
                  "sni-level-1",
                  "sni-level-2",
                  "ssyk-level-4",
                  "wage-type",
                  "worktime-extent"}

                "concept type"))

(defn handle-result-with-error [result error-message]
  (if result
    {:status 200 :body (types/map->nsmap  result)}
    {:status 404 :body (types/map->nsmap {:error error-message})}))

(defn handle-result
  ([result id]
   (handle-result-with-error result (str "No new id found for " id)))
  ([result id type]
   (handle-result-with-error result (str "No new id found for " id  " with type " type))))

(defn convert-matching-component-id-to-new-id-handler [request]
  (let [{:keys [matching-component-id type]}  (pu/get-query-from-request request)
        _ (log/info (str "GET" "/convert-matching-component-id-to-new-id" matching-component-id " " type))
        result (get-concept-id-from-matching-component-id matching-component-id type)]
    (handle-result result matching-component-id type)))

(defn convert-old-id-to-new-id-handler [request]
  (let [{:keys [legacy-id type]} (pu/get-query-from-request request)
        _                        (log/info (str "GET" "/convert-old-id-to-new-id" legacy-id " " type))
        result (get-concept-id-from-legacy-id legacy-id type)]
    (handle-result result legacy-id type)))

(defn convert-ssyk-2012-code-to-new-id-handler [request]
  (let [{:keys [code]} (pu/get-query-from-request request)
        _ (log/info (str "GET" "/convert-ssyk-2012-code-to-new-id " code))
        result (get-concept-id-from-ssyk-2012  code)]
    (handle-result result code)))

(defn convert-municipality-code-to-new-id-handler [request]
  (let [{:keys [code]} (pu/get-query-from-request request)
        _ (log/info (str "GET"  "/convert-municipality-code-to-new-id"  code))
        result (get-concept-id-from-municipality-code  code)]
    (handle-result result code)))

(defn convert-swedish-region-code-to-new-id [request]
  (let [{:keys [code]} (pu/get-query-from-request request)
        _  (log/info (str "GET"   "/convert-swedish-region-code-to-new-id" code))
        result (get-concept-id-from-swedish-region-code  code)]
    (handle-result result code)))

(defn convert-new-id-to-old-id-handler [request]
  (let [{:keys [id]} (pu/get-query-from-request request)
        _  (log/info (str "GET"   "/convert-new-id-to-old-id-handler" id))
        result (get-legacy-concept-from-new-id id)]
    (handle-result result id)))
