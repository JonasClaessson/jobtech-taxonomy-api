(ns jobtech-taxonomy-api.db.daynotes
  (:refer-clojure :exclude [type])
  (:require [datomic.client.api :as d]
            [jobtech-taxonomy-api.db.database-connection :refer :all]
            [jobtech-taxonomy-api.db.concepts :as concepts]
            [jobtech-taxonomy-api.db.api-util :as api-util]
            [jobtech-taxonomy-api.config :refer [env]]
            [mount.core :refer [defstate]]))

;; TODO Investigate if we should have custom attributes for created updated at

(defn- conj-in [m & path+vals]
  (->> path+vals
       (partition 2)
       (reduce #(update-in %1 (first %2) conj (second %2)) m)))

(defn- fetch-relation-txs [hist-db concept-id from-timestamp to-timestamp]
  (-> {:query '{:find [?tx ?inst ?user-id ?comment ?r]
                :keys [tx timestamp user-id comment relation]
                :in [$]
                :where [[?c :concept/id ?input-concept-id]
                        (or [?r :relation/concept-1 ?c]
                            [?r :relation/concept-2 ?c])
                        (or [?r _ _ ?tx]
                            [?tx :daynote/ref ?r])
                        [(get-else $ ?tx :taxonomy-user/id "@system") ?user-id]
                        [(get-else $ ?tx :daynote/comment false) ?comment]
                        [?tx :db/txInstant ?inst]]}
       :args [hist-db]}
      (cond->
       concept-id
        (conj-in [:query :in] '?input-concept-id
                 [:args] concept-id)

        from-timestamp
        (conj-in [:query :in] '?from-inst
                 [:query :where] '[(<= ?from-inst ?inst)]
                 [:args] from-timestamp)

        to-timestamp
        (conj-in [:query :in] '?to-inst
                 [:query :where] '[(< ?inst ?to-inst)]
                 [:args] to-timestamp))
      d/q))

(defn- fetch-relation-changes [hist-db db tx+relations]
  (group-by (juxt :tx :relation)
            (d/q {:query '[:find ?tx ?r ?t ?added
                           (pull $latest ?c1 [:concept/id :concept/type :concept/preferred-label])
                           (pull $latest ?c2 [:concept/id :concept/type :concept/preferred-label])
                           :keys tx relation relation-type added source target
                           :in $history $latest [[?tx ?r] ...]
                           :where
                           [$history ?r :relation/type ?t ?tx ?added]
                           [$history ?r :relation/concept-1 ?c1]
                           [$history ?r :relation/concept-2 ?c2]]
                  :args [hist-db db tx+relations]})))

(defn get-for-relation [concept-id from-timestamp to-timestamp]
  (let [db (get-db)
        hist-db (get-db-hist db)
        relation-txs (fetch-relation-txs hist-db concept-id from-timestamp to-timestamp)
        tx+relation->changes (fetch-relation-changes hist-db db (map (juxt :tx :relation) relation-txs))]
    (->> relation-txs
         (sort-by :tx)
         (reduce (fn [acc {:keys [tx timestamp user-id comment relation]}]
                   (let [change (first (tx+relation->changes [tx relation]))
                         state (or (not-empty (select-keys change [:relation-type :source :target]))
                                   (get-in acc [:relation->latest-state relation])
                                   (d/pull (d/as-of db tx)
                                           '[(:relation/type :as :relation-type)
                                             {(:relation/concept-1 :as :source) [:concept/id :concept/type :concept/preferred-label]
                                              (:relation/concept-2 :as :target) [:concept/id :concept/type :concept/preferred-label]}]
                                           relation))
                         event (cond-> {:event-type (cond
                                                      (nil? change) "COMMENTED"
                                                      (:added change) "CREATED"
                                                      :else "DEPRECATED")
                                        :timestamp timestamp
                                        :user-id user-id
                                        :relation state}
                                 comment (assoc :comment comment))]
                     (-> acc
                         (update :events conj event)
                         (assoc-in [:relation->latest-state relation] state))))
                 {:relation->latest-state {}
                  :events []})
         :events)))

(defn create-for-relation [user-id comment concept-1 concept-2 relation-type]
  (when-let [e (concepts/fetch-relation-entity-id-from-concept-ids-and-relation-type concept-1 concept-2 relation-type)]
    (d/transact (get-conn) {:tx-data [(api-util/user-action-tx user-id comment)
                                      {:db/id "datomic.tx"
                                       :daynote/ref e}]})))

(defn- fetch-concept-txs [hist-db concept-id from-timestamp to-timestamp]
  (-> {:query '{:find [?tx ?inst ?user ?comment ?c ?id]
                :keys [tx inst user comment concept concept-id]
                :in [$]
                :where [[?c :concept/id ?id]
                        (or [?c _ _ ?tx]
                            [?tx :daynote/ref ?c])
                        [?tx :db/txInstant ?inst]
                        [(get-else $ ?tx :taxonomy-user/id "@system") ?user]
                        [(get-else $ ?tx :daynote/comment false) ?comment]]}
       :args [hist-db]}
      (cond->
       concept-id
        (conj-in [:query :in] '?id
                 [:args] concept-id)

        from-timestamp
        (conj-in [:query :in] '?from-inst
                 [:query :where] '[(<= ?from-inst ?inst)]
                 [:args] from-timestamp)

        to-timestamp
        (conj-in [:query :in] '?to-inst
                 [:query :where] '[(< ?inst ?to-inst)]
                 [:args] to-timestamp))
      d/q))

(defn- fetch-concept-changes [hist-db tx+concepts]
  (group-by (juxt :tx :concept)
            (d/q '[:find ?tx ?c ?attr ?v ?added
                   :keys tx concept a v added
                   :in $ [[?tx ?c] ...]
                   :where
                   [?c ?a ?v ?tx ?added]
                   [?a :db/ident ?attr]]
                 hist-db tx+concepts)))

(defn- changes->deprecated-event [changes]
  (when (some #(and (:added %) (#{:concept/deprecated :concept/replaced-by} (:a %)) (:v %)) changes)
    {:event-type "DEPRECATED"}))

(defn- changes->commented-event [changes]
  (when (empty? changes)
    {:event-type "COMMENTED"}))

(defn- changes->created-event [changes]
  (when (some #(and (:added %) (= :concept/id (:a %))) changes)
    {:event-type "CREATED"
     :concept (->> changes
                   (map (juxt :a :v))
                   (into {}))}))

(defn- changes->updated-event [changes]
  {:event-type "UPDATED"
   :changes (->> changes
                 (group-by :a)
                 (map (fn [[attr changes]]
                        {:attribute attr
                         :old-value (->> changes (remove :added) first :v)
                         :new-value (->> changes (filter :added) first :v)})))})

(defn changes->event [changes]
  (or (changes->commented-event changes)
      (changes->deprecated-event changes)
      (changes->created-event changes)
      (changes->updated-event changes)))

(defn get-for-concept [concept-id from-timestamp to-timestamp]
  (let [hist-db (d/history (get-db))
        daynote-txs (fetch-concept-txs hist-db concept-id from-timestamp to-timestamp)
        tx+concept->changes (fetch-concept-changes hist-db (map (juxt :tx :concept) daynote-txs))]
    (->> daynote-txs
         (sort-by :tx)
         (map (fn [{:keys [tx concept inst user comment concept-id]}]
                (-> [tx concept]
                    tx+concept->changes
                    changes->event
                    (assoc :timestamp inst :user-id user :concept-id concept-id)
                    (cond-> comment (assoc :comment comment))))))))

(defn create-for-concept [concept-id user-id comment]
  (d/transact (get-conn) {:tx-data [(api-util/user-action-tx user-id comment)
                                    {:db/id "datomic.tx"
                                     :daynote/ref [:concept/id concept-id]}]}))