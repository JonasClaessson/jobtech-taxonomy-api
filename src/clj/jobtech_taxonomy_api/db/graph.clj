(ns jobtech-taxonomy-api.db.graph
  (:refer-clojure :exclude [type])
  (:require [datomic.client.api :as d]
            [jobtech-taxonomy-api.db.database-connection :refer :all]
            [jobtech-taxonomy-api.db.api-util :refer :all]
            [clojure.set :as set]))

(def initial-graph-query
  {:query '{:find [(pull ?r [:relation/type
                             :relation/substitutability-percentage
                             {:relation/concept-1 [:concept/id]
                              :relation/concept-2 [:concept/id]}])
                   (pull ?c1 [:concept/id
                              :concept/deprecated
                              :concept/preferred-label
                              :concept/type])
                   (pull ?c2 [:concept/id
                              :concept/deprecated
                              :concept/preferred-label
                              :concept/type])]
            :in [$]
            :where []}
   :args []})

(defn handle-relations [query relation concept-1-type concept-2-type]
  {:pre [(contains? #{"broader" "narrower" "related" "substitutability"} relation)]}
  (case relation
    "broader"
    (-> query
        (update-in [:query :in] into '[?relation-type ?c1-type ?c2-type])
        (update :args into [relation concept-1-type concept-2-type])
        (update-in [:query :where] into '[[?r :relation/type ?relation-type]
                                          [?r :relation/concept-1 ?c1]
                                          [?r :relation/concept-2 ?c2]
                                          [?c1 :concept/type ?c1-type]
                                          [?c2 :concept/type ?c2-type]]))

    "narrower"
    (-> query
        (update-in [:query :in] into '[?relation-type ?c1-type ?c2-type])
        (update :args into ["broader" concept-1-type concept-2-type])
        (update-in [:query :where] into '[[?r :relation/type ?relation-type]
                                          [?r :relation/concept-1 ?c1]
                                          [?r :relation/concept-2 ?c2]
                                          [?c1 :concept/type ?c2-type]
                                          [?c2 :concept/type ?c1-type]]))

    "related"
    (-> query
        (update-in [:query :in] into '[?relation-type ?c1-type ?c2-type])
        (update :args into [relation concept-1-type concept-2-type])
        (update-in [:query :where] into '[[?r :relation/type ?relation-type]
                                          (or (and [?r :relation/concept-1 ?c1]
                                                   [?r :relation/concept-2 ?c2])
                                              (and [?r :relation/concept-1 ?c2]
                                                   [?r :relation/concept-2 ?c1]))
                                          [?c1 :concept/type ?c2-type]
                                          [?c2 :concept/type ?c1-type]]))

    "substitutability"
    (-> query
        (update-in [:query :in] into '[?relation-type ?c1-type ?c2-type])
        (update :args into [relation concept-1-type concept-2-type])
        (update-in [:query :where] into '[[?r :relation/type ?relation-type]
                                          [?r :relation/concept-1 ?c1]
                                          [?r :relation/concept-2 ?c2]
                                          [?c1 :concept/type ?c1-type]
                                          [?c2 :concept/type ?c2-type]]))))

(defn- exclude-deprecated [query]
  (update-in query [:query :where] into '[[(get-else $ ?c1 :concept/deprecated false) ?c1-deprecated]
                                          [(ground false) ?c1-deprecated]
                                          [(get-else $ ?c2 :concept/deprecated false) ?c2-deprecated]
                                          [(ground false) ?c2-deprecated]]))

(defn build-graph-query [relation-type concept-1-type concept-2-type include-deprecated offset limit version]
  {:pre [relation-type concept-1-type concept-2-type]}
  (-> initial-graph-query
      (update :args conj (get-db version))
      (handle-relations relation-type concept-1-type concept-2-type)
      (cond-> (not include-deprecated) exclude-deprecated)
      (cond-> offset (assoc :offset offset))
      (cond-> limit (assoc :limit limit))))

(def initial-graph-response
  {:taxonomy/graph
   {:taxonomy/nodes #{}
    :taxonomy/edges #{}}})

(defn create-edge [{:relation/keys [concept-1 concept-2 type substitutability-percentage]}]
  (cond-> {:taxonomy/source (:concept/id concept-1)
           :taxonomy/target (:concept/id concept-2)
           :taxonomy/relation-type type}
    substitutability-percentage
    (assoc :taxonomy/substitutability-percentage substitutability-percentage)))

(defn create-node [concept]
  (set/rename-keys concept {:concept/id :taxonomy/id
                            :concept/preferred-label :taxonomy/preferred-label
                            :concept/type :taxonomy/type
                            :concept/deprecated :taxonomy/deprecated}))

(defn db-graph-response-reducer [acc [relation-data concept-1 concept-2]]
  (-> acc
      (update-in [:taxonomy/graph :taxonomy/edges] conj (create-edge relation-data))
      (update-in [:taxonomy/graph :taxonomy/nodes] conj (create-node concept-1))
      (update-in [:taxonomy/graph :taxonomy/nodes] conj (create-node concept-2))))

(defn fetch-graph [relation-type source-concept-type target-concept-type include-deprecated offset limit version]
  (reduce db-graph-response-reducer initial-graph-response
          (d/q (build-graph-query relation-type source-concept-type target-concept-type include-deprecated offset limit version))))
