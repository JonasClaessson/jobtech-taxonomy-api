(ns jobtech-taxonomy-api.db.database-connection
  (:require [datomic.client.api :as d]
            [mount.core :refer [defstate]]
            [jobtech-taxonomy-api.config :refer [env]]))

(defn get-client []
  (d/client (:datomic-cfg env)))

(defn get-conn []
  (d/connect (get-client) {:db-name (:datomic-name env)}))

(defstate ^{:on-reload :noop} conn
  :start (do (println "start:conn" (:datomic-name env))
             (get-conn)))

(def get-database-time-point-by-version-query
  '[:find ?tx
    :in $ ?version
    :where
    [?t :taxonomy-version/id ?version]
    [?t :taxonomy-version/tx ?tx]])

(defn get-transaction-id-from-version [db version]
  (ffirst (d/q get-database-time-point-by-version-query db version)))

(def get-latest-released-version-query
  '[:find (max ?version)
    :in $
    :where [_ :taxonomy-version/id ?version]])

(defn get-latest-released-version [db]
  (ffirst (d/q get-latest-released-version-query db)))

;; This cannot use the conn var, as it will destroy the
;; integration tests (where the conn is made before the
;; tests fill the databases with test data).
(defn get-db
  "Get database, possibly at a point in time of some release.

  Optional version arg can be:
  - `:next` (default) - the latest database, includes unpublished changes
  - `:latest` - the database at a latest release time point
  - an int - the database at a time point of that release"
  ([]
   (get-db :next))
  ([version]
   (let [db (d/db (get-conn))]
     (cond
       (= :next version)
       db

       (= :latest version)
       (->> (or (get-latest-released-version db)
                (throw (ex-info "There are no releases yet"
                                {:db db})))
            (get-transaction-id-from-version db)
            (d/as-of db))

       (int? version)
       (d/as-of db (or (get-transaction-id-from-version db version)
                       (throw (ex-info "There is no such db version"
                                       {:version version
                                        :db db}))))

       :else
       (throw (ex-info "Invalid db version, use :latest, :next or int"
                       {:version version}))))))

(defn get-db-hist [db]
  (d/history db))