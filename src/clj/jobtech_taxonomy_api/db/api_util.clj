(ns jobtech-taxonomy-api.db.api-util)

(defn parse-seach-concept-datomic-result [result]
  (map first result))

(defn pagination
  [coll offset limit]
  (cond
    (and coll offset limit) (take limit (drop offset coll))
    (and coll limit) (take limit coll)
    (and coll offset) (drop offset coll)
    :else coll))

(def regex-char-esc-smap
  (let [esc-chars "()&^%$#!?*.+"]
    (zipmap esc-chars
            (map #(str "\\" %) esc-chars))))

(defn ignore-case [string]
  (str "(?iu:" string ")"))

(defn ignore-case-eager [string]
  (str "(?iu:" string ".*)"))

(defn str-to-pattern-lazy
  [string]
  (->> string
       (replace regex-char-esc-smap)
       (reduce str)
       ignore-case))

(defn str-to-pattern-eager
  [string]
  (->> string
       (replace regex-char-esc-smap)
       (reduce str)
       ignore-case-eager))

(defn user-action-tx [user-id comment]
  (cond-> {:db/id "datomic.tx"
           :taxonomy-user/id user-id}
    comment (assoc :daynote/comment comment)))
