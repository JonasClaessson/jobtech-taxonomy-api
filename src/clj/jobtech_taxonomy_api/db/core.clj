(ns jobtech-taxonomy-api.db.core
  (:require [datomic.client.api :as d]
            [jobtech-taxonomy-api.db.database-connection :refer :all]
            [jobtech-taxonomy-api.db.api-util :refer :all]
            [jobtech-taxonomy-api.db.concepts :as concepts]
            [clojure.tools.logging :as log]))

(defn retract-concept [user-id id comment]
  (when (concepts/exists? (get-db) id)
    (let [result (d/transact (get-conn) {:tx-data [(user-action-tx user-id comment)
                                                   {:concept/id id
                                                    :concept/deprecated true}]})]
      (log/info result)
      result)))

(def get-relation-types-query
  '[:find ?v :where [_ :relation/type ?v]])

(defn get-relation-types []
  (->> (d/q get-relation-types-query (get-db))
       (sort-by first)
       (apply concat)))

;; TODO appeda pa replaced by listan

(defn replace-deprecated-concept [user-id old-concept-id new-concept-id comment]
  (let [db (get-db)]
    (when (and (concepts/exists? db old-concept-id)
               (concepts/exists? db new-concept-id))
      (d/transact (get-conn) {:tx-data [(user-action-tx user-id comment)
                                        {:concept/id old-concept-id
                                         :concept/deprecated true
                                         :concept/replaced-by [[:concept/id new-concept-id]]}]}))))

(def get-all-taxonomy-types-query
  '[:find ?v :where [_ :concept/type ?v]])

(defn get-all-taxonomy-types "Return a list of taxonomy types." [version]
  (->> (d/q get-all-taxonomy-types-query (get-db version))
       (sort-by first)
       (flatten)
       (map name)))
