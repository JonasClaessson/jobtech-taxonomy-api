(ns jobtech-taxonomy-api.db.graphql
  (:require [com.walmartlabs.lacinia.schema :as l.schema]
            [com.walmartlabs.lacinia.resolve :as l.resolve]
            [com.walmartlabs.lacinia :as l]
            [jobtech-taxonomy-api.db.database-connection :refer [get-db]]
            [datomic.client.api :as d]
            [urania.core :as urania]
            [promesa.core :as promesa]
            [superlifter.core :as superlifter]
            [clojure.main :as m]
            [jobtech-taxonomy-api.middleware :as middleware]))

;; GraphQL implementation on top of Lacinia. Lacinia does all the query parsing, while all we need to do is provide
;; data resolvers for declared fields. Data resolver is a function that takes 3 arguments:
;; - ctx — lacinia context map;
;; - args — query arguments for this field;
;; - value — value that we want to know more about.
;; It should return resolved value.
;; Queries on the root level receive nil value. Concept query fetches only very shallow concepts, containing only `:id`
;; key, and other fields like `:type` fetch concept types by that id.
;; This implementation uses Superlifter to batch fetches for fields of different concepts into single query, grouping
;; them by multi-resolver and its args. Multi-resolver is function similar to resolver, with difference being:
;; - 3rd argument is a list of values we want to know more about;
;; - it should return a list of resolved values.

(defn- into-in [m & path+vals]
  (->> path+vals
       (partition 2)
       (reduce #(update-in %1 (first %2) into (second %2)) m)))

(defn- concepts [db {:keys [id type include_deprecated limit offset]}]
  (d/q (-> {:query '{:find [?id]
                     :keys [id]
                     :in [$]
                     :where []}
            :args [db]}
           (cond->
            id (into-in [:args] [id]
                        [:query :in] '[[?id ...]]
                        [:query :where] '[[?c :concept/id ?id]])
            type (into-in [:args] [type]
                          [:query :in] '[[?type ...]]
                          [:query :where] '[[?c :concept/type ?type]])
            (not include_deprecated) (into-in [:query :where] '[[(ground false) ?deprecated]
                                                                [(get-else $ ?c :concept/deprecated false) ?deprecated]])
            limit (assoc :limit limit)
            offset (assoc :offset offset))
           (into-in [:query :where] '[[?c :concept/id ?id]]))))

(defn- inject-db
  "Resolver middleware that authenticates user and injects database for required version"
  [f]
  (fn [{::keys [api-key] :as ctx} {:keys [version] :as args} value]
    (cond
      (not (or (middleware/authenticate-user api-key)
               (middleware/authenticate-admin api-key)))
      (l.resolve/with-error [] {:message "Not authorized"})

      (and (= "next" version) (not (middleware/authenticate-admin api-key)))
      (l.resolve/with-error [] {:message "Not authorized"})

      :else

      (let [version (or ({"latest" :latest "next" :next} version)
                        (Integer/parseInt version))
            db (get-db version)]
        (l.resolve/with-context (f (assoc ctx ::db db) args value)
          {::db db})))))

(defn- catch-multi-fetch-exceptions [fetch]
  (fn [ctx args values]
    (try
      (fetch ctx args values)
      (catch Exception e
        (promesa/resolved (repeat e))))))

(defn- fetcher [id fetch args]
  (with-meta (reify
               urania/DataSource
               (-identity [_] id)
               (-fetch [_ env]
                 (promesa/then ((catch-multi-fetch-exceptions fetch) env args [id]) first))
               urania/BatchedSource
               (-fetch-multi [this resources env]
                 (let [ids (map urania/-identity (cons this resources))]
                   (promesa/then ((catch-multi-fetch-exceptions fetch) env args ids) #(zipmap ids %)))))
    ;; Urania batches received requests by `#(pr-str (type %))`. Some tools (like cider) override function's string
    ;; representation in a way that breaks this batching (they exclude identity hash code for readability). To make
    ;; batching reliable, we ensure proper string representation of this data source by explicitly including function's
    ;; identity hash code
    {:type [(.getName (class fetch)) (System/identityHashCode fetch) args]}))

(defn- batch-resolver
  "Resolver middleware that converts multi-resolver to a single field resolver

  When query is executed, it batches multiple requests to returned single resolver to a single request to the wrapped
  multi-resolver, thus solving N+1 problem"
  [fetch]
  (fn [ctx args value]
    (let [superlifter (::superlifter ctx)
          p (l.resolve/resolve-promise)]
      (-> superlifter
          (superlifter/enqueue! (fetcher value fetch args))
          (promesa/then (fn [result]
                          (if (instance? Exception result)
                            (l.resolve/deliver! p nil {:message (-> result Throwable->map m/ex-triage m/ex-str)})
                            (l.resolve/deliver! p result)))))
      p)))

(defn- fetch-concepts-attr
  ([attr]
   (fetch-concepts-attr attr nil))
  ([attr default]
   (fn [ctx _ concepts]
     (let [db (::db ctx)
           ids (map :id concepts)
           id->value (into {} (d/q '[:find ?id ?v
                                     :in $ [?id ...] ?a
                                     :where
                                     [?c :concept/id ?id]
                                     [?c ?a ?v]]
                                   db ids attr))]
       (map #(id->value % default) ids)))))

(defn- fetch-legacy-ids [ctx _ concepts]
  (let [db (::db ctx)
        ids (map :id concepts)
        id->value (into {} (d/q '[:find ?id ?legacy-id
                                  :in $ [?id ...]
                                  :where
                                  [?c :concept/id ?id]
                                  [?c :concept.external-database.ams-taxonomy-67/id ?legacy-id]]
                                db ids))]
    (map id->value ids)))

(defn- make-relation-fetcher
  "Factory for relation multi-resolvers

  `enrich-query` that is expected to modify passed query to select related concept entities. It can expect `?c` being
  bound to source concept, and returned query needs to bind `?related-c`"
  [enrich-query]
  (fn [ctx {:keys [type include_deprecated]} concepts]
    (let [db (::db ctx)
          ids (map :id concepts)
          id->concepts (-> {:query '{:find [?from-id ?id]
                                     :keys [from_id id]
                                     :in [$ [?from-id ...]]
                                     :where [[?c :concept/id ?from-id]]}
                            :args [db ids]}
                           enrich-query
                           (cond->
                            type (into-in [:args] [type]
                                          [:query :in] '[?type]
                                          [:query :where] '[[?related-c :concept/type ?type]])
                            (not include_deprecated) (into-in [:query :where] '[[(ground false) ?deprecated]
                                                                                [(get-else $ ?related-c :concept/deprecated false) ?deprecated]]))
                           (into-in [:query :where] '[[?related-c :concept/id ?id]])
                           d/q
                           (->> (group-by :from_id)))]
      (mapv #(id->concepts % []) ids))))

(def ^:private fetch-related
  (make-relation-fetcher
   #(update-in % [:query :where] into '[(or (and [?r :relation/concept-1 ?c]
                                                 [?r :relation/concept-2 ?related-c])
                                            (and [?r :relation/concept-2 ?c]
                                                 [?r :relation/concept-1 ?related-c]))
                                        [?r :relation/type "related"]])))

(def ^:private fetch-narrower
  (make-relation-fetcher
   #(update-in % [:query :where] into '[[?r :relation/concept-2 ?c]
                                        [?r :relation/type "broader"]
                                        [?r :relation/concept-1 ?related-c]])))

(def ^:private fetch-broader
  (make-relation-fetcher
   #(update-in % [:query :where] into '[[?r :relation/concept-1 ?c]
                                        [?r :relation/type "broader"]
                                        [?r :relation/concept-2 ?related-c]])))

(def ^:private fetch-substitutes
  (make-relation-fetcher
   #(into-in %
             [:query :where] '[[?r :relation/concept-2 ?c]
                               [?r :relation/type "substitutability"]
                               [?r :relation/concept-1 ?related-c]
                               [?r :relation/substitutability-percentage ?substitutability]]
             [:query :find] '[?substitutability]
             [:query :keys] '[:substitutability_percentage])))

(def ^:private fetch-substituted-by
  (make-relation-fetcher
   #(into-in %
             [:query :where] '[[?r :relation/concept-1 ?c]
                               [?r :relation/type "substitutability"]
                               [?r :relation/concept-2 ?related-c]
                               [?r :relation/substitutability-percentage ?substitutability]]
             [:query :find] '[?substitutability]
             [:query :keys] '[:substitutability_percentage])))

(def ^:private fetch-replaced-by
  (make-relation-fetcher
   #(into-in % [:query :where] '[[?c :concept/replaced-by ?related-c]])))

(def ^:private fetch-replaces
  (make-relation-fetcher
   #(into-in % [:query :where] '[[?related-c :concept/replaced-by ?c]])))

(defn- inject-superlifter
  "Resolver middleware that injects superlifter to execution context, allowing to use batch resolvers

  It also puts `::stop-superlifter` extension function that needs to be called after query is executed to shutdown
  superlifter fetch triggers"
  [f]
  (fn [ctx args value]
    (let [superlifter (superlifter/start!
                       {:buckets {:default {:triggers {:interval {:interval 1}}}}
                        :urania-opts {:env ctx}})]
      (-> (f (assoc ctx ::superlifter superlifter) args value)
          (l.resolve/with-context {::superlifter superlifter})
          (l.resolve/with-extensions assoc ::stop-superlifter #(superlifter/stop! superlifter))))))

(defn- catch-exceptions
  "Resolver middleware that catches exceptions and converts them to GraphQL errors"
  [f]
  (fn [ctx args value]
    (try
      (f ctx args value)
      (catch Exception e
        (l.resolve/with-error nil {:message (-> e Throwable->map m/ex-triage m/ex-str)})))))

(def ^:private schema
  (l.schema/compile
   {:queries {:concepts {:type '(non-null (list (non-null :Concept)))
                         :description "Fetch concepts"
                         :args {:id {:type '(list (non-null String))
                                     :description "Restrict results to these concept IDs"}
                                :type {:type '(list (non-null String))
                                       :description "Restrict results to these concept types"}
                                :limit {:type 'Int
                                        :description "Pagination: maximum amount of returned concepts"}
                                :offset {:type 'Int
                                         :description "Pagination: skip this many returned concepts"}
                                :include_deprecated {:type '(non-null Boolean)
                                                     :description "Include deprecated concepts"
                                                     :default-value false}
                                :version {:type '(non-null String)
                                          :description "Use this taxonomy version, valid values are `\"latest\"` for latest release, `\"next\"` for unpublished changes (requires admin rights) or number indicating the version"
                                          :default-value "latest"}}
                         :resolve (catch-exceptions
                                   (inject-db
                                    (inject-superlifter
                                     (fn [ctx args _]
                                       (concepts (::db ctx) args)))))}}
    :objects
    {:Concept
     {:description "Fundamental taxonomy type"
      :fields
      {:id {:type '(non-null String)
            :description "ID of a concept"}
       :type {:type '(non-null String)
              :description "Type of a concept"
              :resolve (batch-resolver (fetch-concepts-attr :concept/type))}
       :preferred_label {:type '(non-null String)
                         :description "Textual name of a concept"
                         :resolve (batch-resolver (fetch-concepts-attr :concept/preferred-label))}
       :definition {:type '(non-null String)
                    :description "Concept definition"
                    :resolve (batch-resolver (fetch-concepts-attr :concept/definition))}
       :quality_level {:type 'Int
                       :description "quality level of this concept (1, 2 or 3)"
                       :resolve (batch-resolver (fetch-concepts-attr :concept/quality-level))}
       :ssyk_code_2012 {:type 'String
                        :description "SSYK code that can be found on concepts of `ssyk-level-*` types"
                        :resolve (batch-resolver (fetch-concepts-attr :concept.external-standard/ssyk-code-2012))}
       :isco_code_08 {:type 'String
                      :description "ISCO code that can be found on concepts of `isco-level-4` type"
                      :resolve (batch-resolver (fetch-concepts-attr :concept.external-standard/isco-code-08))}
       :eures_code_2014 {:type 'String
                         :description "EURES code that can be found on concepts of `employment-duration` type"
                         :resolve (batch-resolver (fetch-concepts-attr :concept.external-standard/eures-code-2014))}
       :driving_licence_code_2013 {:type 'String
                                   :description "Driver licence code that can be found on concepts of `driving-licence` type"
                                   :resolve (batch-resolver (fetch-concepts-attr :concept.external-standard/driving-licence-code-2013))}
       :nuts_level_3_code_2013 {:type 'String
                                :description "NUTS level 3 code that can be found on concepts of `region` type"
                                :resolve (batch-resolver (fetch-concepts-attr :concept.external-standard/nuts-level-3-code-2013))}
       :national_nuts_level_3_code_2019 {:type 'String
                                         :description "Swedish Län code that can be found on concepts of `region` type"
                                         :resolve (batch-resolver (fetch-concepts-attr :concept.external-standard/national-nuts-level-3-code-2019))}
       :lau_2_code_2015 {:type 'String
                         :description "Swedish Municipality code that can be found on concepts of `municipality` type"
                         :resolve (batch-resolver (fetch-concepts-attr :concept.external-standard/lau-2-code-2015))}
       :iso_3166_1_alpha_2_2013 {:type 'String
                                 :description "2 letter country code"
                                 :resolve (batch-resolver (fetch-concepts-attr :concept.external-standard/iso-3166-1-alpha-2-2013))}
       :iso_3166_1_alpha_3_2013 {:type 'String
                                 :description "3 letter country code"
                                 :resolve (batch-resolver (fetch-concepts-attr :concept.external-standard/iso-3166-1-alpha-3-2013))}
       :iso_639_3_alpha_2_2007 {:type 'String
                                :description "2 letter language code"
                                :resolve (batch-resolver (fetch-concepts-attr :concept.external-standard/iso-639-3-alpha-2-2007))}
       :iso_639_3_alpha_3_2007 {:type 'String
                                :description "3 letter language code"
                                :resolve (batch-resolver (fetch-concepts-attr :concept.external-standard/iso-639-3-alpha-3-2007))}
       :sun_education_field_code_2020 {:type 'String
                                       :description "SUN education field code that can be found on concepts of `sun-education-field-*` types"
                                       :resolve (batch-resolver (fetch-concepts-attr :concept.external-standard/sun-education-field-code-2020))}
       :sun_education_level_code_2020 {:type 'String
                                       :description "SUN education level code that can be found on concepts of `sun-education-level-*` types"
                                       :resolve (batch-resolver (fetch-concepts-attr :concept.external-standard/sun-education-level-code-2020))}
       :sni_level_code_2007 {:type 'String
                             :description "SNI level code that can be found on concepts of `sni-level-*` types"
                             :resolve (batch-resolver (fetch-concepts-attr :concept.external-standard/sni-level-code-2007))}
       :deprecated {:type '(non-null Boolean)
                    :description "Indicator whether the concept is deprecated"
                    :resolve (batch-resolver (fetch-concepts-attr :concept/deprecated false))}
       :deprecated_legacy_id {:type 'String
                              :deprecated "This value is needed for Arbetsförmedlingen's internal systems during migration period. It will be removed."
                              :description "Concept ID that was imported from the old Arbetsförmedlingen's taxonomy. Uniqueness is per type only"
                              :resolve (batch-resolver fetch-legacy-ids)}
       :substitutability_percentage {:type 'Int
                                     :description "Substitutability percentage, an int between 0 and 100. Can appear only on concepts inside `substitutes` or `substituted_by` scopes"}
       :related {:type '(non-null (list (non-null :Concept)))
                 :description "Related concepts"
                 :args {:type {:type 'String
                               :description "Restrict results to this concept type"}
                        :include_deprecated {:type '(non-null Boolean)
                                             :description "Include deprecated concepts"
                                             :default-value false}}
                 :resolve (batch-resolver fetch-related)}
       :broader {:type '(non-null (list (non-null :Concept)))
                 :description "Broader concepts"
                 :args {:type {:type 'String
                               :description "Restrict results to this concept type"}
                        :include_deprecated {:type '(non-null Boolean)
                                             :description "Include deprecated concepts"
                                             :default-value false}}
                 :resolve (batch-resolver fetch-broader)}
       :narrower {:type '(non-null (list (non-null :Concept)))
                  :description "Narrower concepts"
                  :args {:type {:type 'String
                                :description "Restrict results to this concept type"}
                         :include_deprecated {:type '(non-null Boolean)
                                              :description "Include deprecated concepts"
                                              :default-value false}}
                  :resolve (batch-resolver fetch-narrower)}
       :substitutes {:type '(non-null (list (non-null :Concept)))
                     :description "Concepts this concept might substitute (results may include `substitutability_percentage`)"
                     :args {:type {:type 'String
                                   :description "Restrict results to this concept type"}
                            :include_deprecated {:type '(non-null Boolean)
                                                 :description "Include deprecated concepts"
                                                 :default-value false}}
                     :resolve (batch-resolver fetch-substitutes)}
       :substituted_by {:type '(non-null (list (non-null :Concept)))
                        :description "Concepts that might substitute this concept (results may include `substitutability_percentage`)"
                        :args {:type {:type 'String
                                      :description "Restrict results to this concept type"}
                               :include_deprecated {:type '(non-null Boolean)
                                                    :description "Include deprecated concepts"
                                                    :default-value false}}
                        :resolve (batch-resolver fetch-substituted-by)}
       :replaces {:type '(non-null (list (non-null :Concept)))
                  :description "Concepts this concept replaces (that are most probably deprecated, so this field includes deprecated concepts by default)"
                  :args {:type {:type 'String
                                :description "Restrict results to this concept type"}
                         :include_deprecated {:type '(non-null Boolean)
                                              :description "Include deprecated concepts"
                                              :default-value true}}
                  :resolve (batch-resolver fetch-replaces)}
       :replaced_by {:type '(non-null (list (non-null :Concept)))
                     :description "Concepts that replace this concept (that is most probably deprecated: don't forget to add `include_deprecated` argument when querying it)"
                     :args {:type {:type 'String
                                   :description "Restrict results to this concept type"}
                            :include_deprecated {:type '(non-null Boolean)
                                                 :description "Include deprecated concepts"
                                                 :default-value false}}
                     :resolve (batch-resolver fetch-replaced-by)}}}}}))

(defn execute [query variables operation-name api-key]
  (let [result (l/execute schema query variables {::api-key api-key} {:operation-name operation-name})
        stop-superlifter (::stop-superlifter (:extensions result))]
    (when stop-superlifter (stop-superlifter))
    (dissoc result :extensions)))

;; todo model implicit driver licenses using relations?
;; todo can we optimise performing multiple queries on a single layer by performing single query? Like loading all concept types at once...