(ns jobtech-taxonomy-api.config
  (:require [cprop.core :refer [load-config]]
            [cprop.source :as source]
            [mount.core :refer [args defstate]]))

(def test-prefix "/test")

(defn test-env!? []
  (= (System/getenv "DATOMIC_CFG__REGION") "eu-west-1"))

(defn make-config []
  (let [base-list [(args)
                   (source/from-system-props)
                   (source/from-env)]
        merged-conf (load-config :merge base-list)
        inttest-db (System/getProperty "integration-test-db")
        checked-test-list (if (nil? inttest-db)
                            base-list
                            [(assoc merged-conf :datomic-name inttest-db)])]
    (load-config
     :merge
     checked-test-list)))

(defstate env
  :start
  (make-config))
