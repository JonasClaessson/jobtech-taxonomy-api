(ns jobtech-taxonomy-api.webhooks
  (:require [jobtech-taxonomy-api.config :refer [env]]
            [jobtech-taxonomy-api.store :as store]
            [clojure.tools.logging :as log]
            [clj-http.client :as client]
            [clojure.core.async :as async :refer [<! >!]]))

(defn get-client-list-from-conf! []
  "Set webhook-clients either in config.edn or the environment, for example:
 webhook-clients='[{:url \"https://postman-echo.com/post\" :headers {}}]'"
  (set (keys (get env :webhook-clients))))

(defn get-client-list-from-store! []
  ""
  (map #(let [key (:key %)
              url (store/store-get key)]
          {:url url :headers {:api-key key}})
       (into [] (store/store-list-keys))))

(defn- send-request! [client version]
  (let [{:keys [url headers]} client
        ch (async/chan 1)
        callback (fn [res]
                   (async/put! ch res)
                   (async/close! ch))]
    (log/info "calling" url)
    (client/post url
                 {:body (format "{\"version\": %d}" version)
                  :headers headers
                  :async true
                  :content-type :json
                  :socket-timeout 1000                      ;; in milliseconds
                  :connection-timeout 1000                  ;; in milliseconds
                  :accept :json}
                 callback
                 callback)
    ch))

(defn- with-retries [delays ch-fn]
  (let [out-ch (async/chan 1)]
    (async/go-loop [delays delays]
      (let [val (<! (ch-fn))]
        (if (instance? Exception val)
          (let [[delay & remaining-delays] delays]
            (if delay
              (do (<! (async/timeout delay))
                  (recur remaining-delays))
              (do (>! out-ch val)
                  (async/close! out-ch))))
          (do (>! out-ch val)
              (async/close! out-ch)))))
    out-ch))

(def ^:private notifier-ch
  "A channel that performs webhook notifications

  Accepts 3-element vectors as inputs:
  1. client
  2. version
  3. out-ch that will receive http response and then will be closed

  Deduplicates incoming requests so only one http request at a time will run per client/version combination"
  (let [;; Input channel receives requests to perform http requests
        ;; these requests are as per doc-string: client, version, out-ch
        input-ch (async/chan)
        ;; Complete channel receives http responses, which are vectors of:
        ;; 1. request id (channel+version)
        ;; 2. http response
        ;; 3. result channel that should receive http response and be closed
        ;; it then puts http response to result channel and removes request mult
        ;; from active requests
        complete-ch (async/chan)
        ;; `schedule-notification` asynchronously performs http request and then
        ;; sends message to complete-ch asking it to forward http response to result
        ;; channel. `schedule-notification` should not send response to result
        ;; channel by itself: it should be done on the same channel that manages
        ;; active request deduplication, otherwise some requests might never
        ;; receive responses.
        schedule-notification (fn [client version]
                                (let [result-ch (async/chan 1)
                                      mult (async/mult result-ch)
                                      id [client version]]
                                  (async/go
                                    (>! complete-ch [id
                                                     (<! (with-retries
                                                           (->> 1000
                                                                (iterate #(* 2 %))
                                                                (map #(min 10000 %))
                                                                (take 7))
                                                           #(send-request! client version)))
                                                     result-ch]))
                                  mult))]
    ;; Main processor loop: receives requests and responses.
    ;; It maintains a map from active requests to mults (to support multiple
    ;; receivers) of response receiver channels.
    (async/go-loop [active-requests {}]
      (let [[val ch] (async/alts! [input-ch complete-ch])]
        (condp = ch
          input-ch
          (let [[client version out-ch] val
                id [client version]
                mult (or (active-requests id)
                         (schedule-notification client version))]
            (async/tap mult out-ch)
            (recur (assoc active-requests id mult)))

          complete-ch
          (let [[id result result-ch] val]
            (>! result-ch result)
            (async/close! result-ch)
            (recur (dissoc active-requests id))))))
    input-ch))

(defn notify!
  "Send POST notification to a client

  Client is a map with following keys:
  - :url - webhook url string
  - :headers - headers map like {:api-key \"some api key\"}

  Version is an int describing new version

  Accepts optional channel out-ch that will receive http response and then will be closed

  Retries on failures. Returns out-ch (a new channel if no channel was supplied)"
  ([client version]
   (notify! client version (async/chan 1)))
  ([client version out-ch]
   (async/put! notifier-ch [client version out-ch])
   out-ch))

(defn notify-all!
  "Send POST notifications to all clients

  Client is a map with following keys:
  - :url - webhook url string
  - :headers - headers map like {:api-key \"some api key\"}

  Version is an int describing new version

  Retries on failures. Returns channel that will receive vector of http responses and then will be closed"
  [clients version]
  (let [ch (async/chan)]
    (async/pipeline-async 256 ch #(notify! %1 version %2) (async/to-chan clients))
    (async/into [] ch)))