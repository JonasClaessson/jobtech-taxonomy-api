(ns jobtech-taxonomy-api.middleware.formats
  (:require
   [cognitect.transit :as transit]
   [luminus-transit.time :as time]
   [muuntaja.core :as m]))

(def instance
  (m/create
   (-> m/default-options
       (update-in
        [:formats "application/transit+json" :decoder-opts]
        (partial merge time/time-deserialization-handlers))
       (update-in
        [:formats "application/transit+json" :encoder-opts]
        (partial merge time/time-serialization-handlers))
       (assoc-in
        [:formats "application/json" :encoder-opts :date-format]
        "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"))))
