(ns jobtech-taxonomy-api.routes.services
  (:refer-clojure :exclude [type])
  (:require [reitit.swagger :as swagger]
            [reitit.swagger-ui :as swagger-ui]
            [reitit.coercion.spec :as spec-coercion]
            [reitit.ring.middleware.muuntaja :as muuntaja]
            [reitit.ring.middleware.multipart :as multipart]
            [reitit.ring.middleware.parameters :as parameters]
            [buddy.auth.accessrules :refer [restrict]]
            [buddy.auth.middleware :refer [wrap-authentication]]
            [buddy.auth :refer [authenticated?]]
            [buddy.auth.http :as http]
            [muuntaja.core :as m]
            [ring.util.http-response :as resp]
            [ring.util.response :as response]
            [jobtech-taxonomy-api.db.graphql :as graphql]
            [jobtech-taxonomy-api.middleware.formats :as formats]
            [jobtech-taxonomy-api.middleware.cors :as cors]
            [jobtech-taxonomy-api.middleware :as middleware]
            [jobtech-taxonomy-api.webhooks :as webhooks]
            [jobtech-taxonomy-api.db.versions :as v]
            [jobtech-taxonomy-api.db.concepts :as concepts]
            [jobtech-taxonomy-api.db.events :as events]
            [jobtech-taxonomy-api.db.changes :as changes]
            [jobtech-taxonomy-api.db.search :as search]
            [jobtech-taxonomy-api.db.graph :as graph]
            [jobtech-taxonomy-api.db.core :as core]
            [jobtech-taxonomy-api.db.daynotes :as daynotes]
            [jobtech-taxonomy-api.webhooks :as webhooks]
            [jobtech-taxonomy-api.store :as store]
            [jobtech-taxonomy-api.config :as config]
            [jobtech-taxonomy-api.db.legacy :as legacy]
            [taxonomy :as types]
            [clojure.tools.logging :as log]
            [clojure.spec.alpha :as s]
            [spec-tools.core :as st]
            [spec-tools.data-spec :as ds]
            [jobtech-taxonomy-api.routes.parameter-util :as pu]
            [jobtech-taxonomy-api.routes.swagger3 :as swagger3]
            [clojure.java.io :as io]))

;; Status:
;;   - fixa replaced-by-modell i /changes, /replaced-by-changes, /search, /private/concept

(defn log-info [message]
  (log/info message))

(defn auth
  "Middleware used in routes that require authentication. If request is not
   authenticated a 401 not authorized response will be returned"
  [handler]
  (fn [request]
    (let [api-key (http/-get-header request "api-key")]
      (if (or (middleware/authenticate-user api-key)
              (middleware/authenticate-admin api-key))
        (handler request)
        (resp/unauthorized (types/map->nsmap {:error "Not authorized"}))))))

(defn authorized-private?
  [handler]
  (fn [request]
    (if (middleware/authenticate-admin (http/-get-header request "api-key"))
      (handler request)
      (resp/unauthorized (types/map->nsmap {:error "Not authorized"})))))

(defn private-query-params
  "Creates middleware that ensures admin authentication for certain query values

  Expects a map from query parameter to a predicate that should be satisfied to trigger
  admin authentication check, for example:
  ```
  {:sudo true?
   :version #{:next}
   :raw-sql-query any?}
  ```
  This param->pred map will require admin rights if either:
  - `:sudo` parameter is `true`
  - `:version` parameter is `:next`
  - `:raw-sql-query` is present"
  [param->pred]
  (let [needs-admin-auth? (apply some-fn
                                 (map (fn [[k pred]]
                                        (fn [query-params]
                                          (let [v (get query-params k ::not-found)]
                                            (and (not= v ::not-found) (pred v)))))
                                      param->pred))]
    (fn [handler]
      (fn [request]
        (if (and (needs-admin-auth? (get-in request [:parameters :query]))
                 (not (middleware/authenticate-admin (http/-get-header request "api-key"))))
          (resp/unauthorized (types/map->nsmap {:error "Not authorized"}))
          (handler request))))))

(defn catch-errors [handler]
  (fn [request]
    (try
      (handler request)
      (catch Exception e
        (log/error e "Error while performing request")
        {:status 500
         :body {::types/error (.getMessage e)}}))))

(defn service-routes []
  [(str (if (config/test-env!?) config/test-prefix "")
        "/v1/taxonomy")
   {:coercion (swagger3/wrap-coercion spec-coercion/coercion)
    :muuntaja formats/instance
    :middleware [catch-errors]
    :swagger {:id ::api
              ;; openapi 3.0 disallows both "swagger" and "openapi" keys in swagger.json
              ;; having non-null values, but `swagger/create-swagger-handler` always sets "swagger" key
              ;; to 2.0. Setting `:swagger` key to `nil` here overrides that key, which allows us
              ;; to satisfy openapi spec (at least in the ui handler?)
              :swagger nil
              :openapi "3.0.0"
              :info {:version "0.10.0"
                     :title "Jobtech Taxonomy"
                     :description "Jobtech taxonomy services."}

              :components {:securitySchemes {:api-key {:type "apiKey" :name "api-key" :in "header"}}}
              :security [{:api-key []}]}}

   ["" {:no-doc true}
    ["/swagger.json"
     {:get (swagger/create-swagger-handler)}]

    ["/swagger-ui*"
     {:get (swagger-ui/create-swagger-ui-handler
            {:url (str (if (config/test-env!?) config/test-prefix "")
                       "/v1/taxonomy/swagger.json")
             :config {:validator-url nil
                      :operationsSorter "alpha"}})}]
    ["/graphiql"
     ["" {:get {:handler (fn [_] (response/url-response (io/resource "public/graphiql.html")))}}]
     [".json" {:get {:handler (constantly (resp/ok {:url (str (if (config/test-env!?) config/test-prefix "")
                                                              "/v1/taxonomy/graphql")}))}}]]]
   ["/graphql"
    {:swagger {:tags ["GraphQL"]}
     :summary "GraphQL endpoint for powerful graph queries"
     :description "You should explore this API in [this GraphQL-specific explorer](/v1/taxonomy/graphiql)"
     :parameters {:query {:query (taxonomy/par string? "query string")
                          (ds/opt :variables) (taxonomy/par string? "json string with query variables")
                          (ds/opt :operationName) (taxonomy/par string? "optional query name that is used when there are multiple query definitions in a single query string")}}
     :middleware [cors/cors]
     :get {:responses {200 {:body any?}}
           :handler (fn [{{{:keys [query variables operationName]} :query} :parameters :as request}]
                      (let [api-key (http/-get-header request "api-key")]
                        {:status 200
                         :body (graphql/execute query
                                                (some->> variables (m/decode formats/instance "application/json"))
                                                operationName
                                                api-key)}))}}]
   ["/main"
    {:swagger {:tags ["Main"]}

     :middleware [cors/cors auth]}

    ["/versions"
     {:summary "Show the available versions."
      :get {:responses {200 {:body types/versions-spec}
                        401 {:body types/unauthorized-spec}
                        500 {:body types/error-spec}}
            :handler (fn [{{{:keys [_]} :query} :parameters}]
                       {:status 200
                        :body (map types/map->nsmap (v/get-all-versions))})}}]

    ["/changes"
     {:summary "Show changes to the taxonomy as a stream of events."
      :parameters {:query {:after-version (taxonomy/par int? "Limit the result to show changes that occured after this version was published."),
                           (ds/opt :to-version-inclusive) (taxonomy/par int? "Limit the result to show changes that occured before this version was published and during this version. (default: latest version)"),
                           (ds/opt :offset) (taxonomy/par int? "Return list offset (from 0)"),
                           (ds/opt :limit) (taxonomy/par int? "Return list limit")}}

      :get {:responses {200 {:body types/events-spec}
                        500 {:body types/error-spec}}
            :handler (fn [{{{:keys [after-version to-version-inclusive offset limit unpublished]} :query} :parameters}]
                       (log/info (str "GET /changes"
                                      " after-version:" after-version
                                      " to-version-inclusive: " to-version-inclusive
                                      " offset: " offset
                                      " limit: " limit))
                       {:status 200
                        :body (let [events (doall (changes/get-all-events-from-version-with-pagination after-version to-version-inclusive offset limit false))
                                    ;; This is needed to squeeze /changes :concept into the same namespace as the other's :concept
                                    renamed (map #(clojure.set/rename-keys % {:concept :changed-concept}) events)]
                                (vec (map types/map->nsmap renamed)))})}}]

    ["/concepts"
     {:summary "Get concepts. Supply at least one search parameter."
      :parameters {:query {(ds/opt :id) (taxonomy/par string? "ID of concept"),
                           (ds/opt :preferred-label) (taxonomy/par string? "Textual name of concept"),
                           (ds/opt :type) (st/spec {:name "types" :spec string? :description "Restrict to concept type"})
                           (ds/opt :deprecated) (taxonomy/par boolean? "Restrict to deprecation state"),
                           (ds/opt :relation) (taxonomy/par #{"broader" "narrower" "related" "substitutability-to" "substitutability-from"} "Relation type. If specified, related-ids is required"),
                           (ds/opt :related-ids) (taxonomy/par string? "OR-restrict to these relation IDs (white space separated list). If specified, relation is required"),
                           (ds/opt :offset) (taxonomy/par int? "Return list offset (from 0)"),
                           (ds/opt :limit) (taxonomy/par int? "Return list limit"),
                           (ds/opt :version) taxonomy/version-param
                           (ds/opt :include-legacy-information) (taxonomy/par boolean? "This parameter will be removed. Include information related to Arbetsförmedlingen's old internal taxonomy"
                                                                              :swagger/deprecated true)}}
      :middleware [(private-query-params {:version #{:next}})]
      :get {:responses {200 {:body types/concepts-spec}
                        500 {:body types/error-spec}}
            :handler (fn [{{{:keys [id preferred-label type deprecated relation
                                    related-ids offset limit version
                                    include-legacy-information]
                             :or {version :latest
                                  include-legacy-information false}} :query} :parameters}]
                       (log/info (str "GET /concepts "
                                      "id:" id
                                      " preferred-label:" preferred-label
                                      " type:" type
                                      " deprecated:" deprecated
                                      " related-ids  " related-ids
                                      " offset:" offset
                                      " limit:" limit))
                       {:status 200
                        :body (vec (map types/map->nsmap (concepts/find-concepts
                                                          {:id id
                                                           :preferred-label preferred-label
                                                           :type (when type (clojure.string/split type #" "))
                                                           :deprecated deprecated
                                                           :relation relation
                                                           :related-ids (when related-ids (clojure.string/split related-ids #" "))
                                                           :offset offset
                                                           :limit limit
                                                           :version version
                                                           :include-legacy-information include-legacy-information})))})}}]

    ["/graph"
     {:summary "Fetch nodes and edges from the Taxonomies. Only one depth is returned at the time."
      :parameters {:query {:edge-relation-type (taxonomy/par string? "Edge relation type")
                           :source-concept-type (taxonomy/par string? "Source nodes concept type")
                           :target-concept-type (taxonomy/par string? "Target nodes concept type")
                           (ds/opt :offset) (taxonomy/par int? "Return list offset (from 0)")
                           (ds/opt :limit) (taxonomy/par int? "Return list limit")
                           (ds/opt :version) taxonomy/version-param
                           (ds/opt :include-deprecated) (taxonomy/par boolean? "Include deprecated concepts")}}
      :middleware [(private-query-params {:version #{:next}})]
      :get {:responses {200 {:body taxonomy/graph-spec}
                        401 {:body types/unauthorized-spec}
                        500 {:body types/error-spec}}
            :handler (fn [{{{:keys [edge-relation-type source-concept-type target-concept-type offset limit version include-deprecated]
                             :or {version :latest}} :query} :parameters}]
                       {:status 200
                        :body (graph/fetch-graph edge-relation-type source-concept-type target-concept-type
                                                 include-deprecated offset limit version)})}}]

    ["/replaced-by-changes"
     {:summary "Show the history of concepts being replaced after a given version."
      :parameters {:query {:after-version (taxonomy/par int? "After what taxonomy version did the change occur"),
                           (ds/opt :to-version-inclusive) (taxonomy/par int? "Limit the result to show changes that occured before this version was published and during this version. (default: latest version)")}}
      :get {:responses {200 {:body types/replaced-by-changes-spec}
                        500 {:body types/error-spec}}
            :handler (fn [{{{:keys [after-version to-version-inclusive]} :query} :parameters}]
                       (log/info (str "GET /replaced-by-changes"
                                      " after-version: " after-version
                                      " to-version-inclusive: " to-version-inclusive))
                       {:status 200
                        :body (vec (map types/map->nsmap (events/get-deprecated-concepts-replaced-by-from-version after-version to-version-inclusive)))})}}]

    ["/concept/types"
     {:summary "Return a list of all taxonomy types."
      :parameters {:query {(ds/opt :version) taxonomy/version-param}}
      :middleware [(private-query-params {:version #{:next}})]
      :get {:responses {200 {:body types/concept-types-spec}
                        500 {:body types/error-spec}}
            :handler (fn [{{{:keys [version]
                             :or {version :latest}} :query} :parameters}]
                       (log/info (str "GET /concept/types version: " version))
                       {:status 200
                        :body (vec (core/get-all-taxonomy-types version))})}}]

    ["/relation/types"
     {:summary "Relation types. Type 'narrower' is not listed here, as it exists as the inverse of 'broader'."
      :get {:responses {200 {:body types/relation-types-spec}
                        500 {:body types/error-spec}}
            :handler (fn [{{{:keys []} :query} :parameters}]
                       (log/info (str "GET /relation/types"))
                       {:status 200
                        :body (vec (core/get-relation-types))})}}]]

   ["/specific"

    {:swagger {:tags ["Specific Types"]
               :description "Exposes concept with detailed information such as codes from external standards."}

     :middleware [cors/cors auth]}

    (map

     #(types/create-detailed-endpoint % log-info concepts/find-concepts)

     types/detailed-endpoint-configs)]

   ["/suggesters"
    {:swagger {:tags ["Suggesters"]}

     :middleware [cors/cors auth]}

    ["/autocomplete"
     {:summary "Autocomplete from query string"
      :description "Help end-users to find relevant concepts from the taxonomy"
      :parameters {:query {:query-string (taxonomy/par string? "String to search for"),
                           (ds/opt :type) (taxonomy/par string? "Type to search for"),
                           (ds/opt :relation) (taxonomy/par #{"broader" "narrower" "related" "substitutability-to" "substitutability-from"} "Relation type"),
                           (ds/opt :related-ids) (taxonomy/par string? "List of related IDs to search for"),
                           (ds/opt :offset) (taxonomy/par int? "Return list offset (from 0)"),
                           (ds/opt :limit) (taxonomy/par int? "Return list limit"),
                           (ds/opt :version) taxonomy/version-param}}
      :middleware [(private-query-params {:version #{:next}})]
      :get {:responses {200 {:body types/autocomplete-spec}
                        500 {:body types/error-spec}}
            :handler (fn [{{{:keys [query-string type relation related-ids offset limit version]
                             :or {version :latest}}
                            :query} :parameters}]

                       (log/info (str "GET /search"
                                      " query-string:" query-string
                                      " type:" type
                                      " offset:" offset
                                      " limit:" limit
                                      " version: " version))

                       {:status 200
                        :body (vec (map types/map->nsmap
                                        (search/get-concepts-by-search
                                         query-string
                                         (when type (clojure.string/split type #" "))
                                         relation
                                         (when related-ids (clojure.string/split related-ids #" "))
                                         offset
                                         limit
                                         version)))})}}]]

   ["/private"
    {:swagger {:tags ["Private"]}
     :middleware [cors/cors auth authorized-private?]}

    ["/changes"
     {:summary "Show changes to the taxonomy as a stream of events. Include unpublished changes."
      :parameters {:query {:after-version (taxonomy/par int? "Limit the result to show changes that occured after this version was published."),
                           (ds/opt :offset) (taxonomy/par int? "Return list offset (from 0)"),
                           (ds/opt :limit) (taxonomy/par int? "Return list limit")}}

      :get {:responses {200 {:body types/events-spec}
                        500 {:body types/error-spec}}
            :handler (fn [{{{:keys [after-version to-version-inclusive offset limit unpublished]} :query} :parameters}]
                       (log/info (str "GET /private/changes"
                                      " after-version:" after-version
                                      " offset: " offset
                                      " limit: " limit))
                       {:status 200
                        :body (let [events (changes/get-all-events-from-version-with-pagination after-version nil offset limit true)
                                    ;; This is needed to squeeze /changes :concept into the same namespace as the other's :concept
                                    renamed (map #(clojure.set/rename-keys % {:concept :changed-concept}) events)]
                                (vec (map types/map->nsmap renamed)))})}}]

    ["/delete-concept"
     {:summary "Retract the concept with the given ID."
      :parameters {:query {:id (taxonomy/par string? "ID of concept")
                           (ds/opt :comment) (taxonomy/par string? "Daynote comment for this action")}}
      :delete {:responses {200 {:body types/ok-spec}
                           404 {:body types/error-spec}
                           500 {:body types/error-spec}}
               :handler (fn [request]
                          (log/info "DELETE /concept")
                          (let [{:keys [id comment]} (pu/get-query-from-request request)
                                user-id (pu/get-user-id-from-request request)]

                            (if (core/retract-concept user-id id comment)
                              {:status 200 :body (types/map->nsmap {:message "ok"})}
                              {:status 404 :body (types/map->nsmap {:error "not found"})})))}}]

    ["/concept"
     {:summary "Assert a new concept."
      :parameters {:query (pu/build-parameter-map [:type :definition :preferred-label :comment :quality-level])}
      :post {:responses {200 {:body types/ok-concept-spec}
                         409 {:body types/error-spec}
                         500 {:body types/error-spec}}
             :handler (fn [request]
                        (let [query (pu/get-query-from-request request)
                              user-id (pu/get-user-id-from-request request)]
                          (log/info "POST /concept")
                          (let [[result timestamp new-concept] (concepts/assert-concept user-id query)]
                            (if result
                              {:status 200 :body (types/map->nsmap {:time timestamp :concept new-concept})}
                              {:status 409 :body (types/map->nsmap {:error "Can't create new concept since it is in conflict with existing concept."})}))))}}]

    ["/concepts"
     {:summary "Get concepts. Supply at least one search parameter."
      :parameters {:query
                   (pu/build-parameter-map [:id :preferred-label :type :deprecated :relation :related-ids :offset :limit :version])}

      :get {:responses {200 {:body types/concepts-spec}
                        500 {:body types/error-spec}}
            :handler (fn [{{{:keys [id preferred-label type deprecated relation
                                    related-ids offset limit version]
                             :or {version :next}} :query} :parameters}]
                       (log/info (str "GET private /concepts "
                                      "id:" id
                                      " preferred-label:" preferred-label
                                      " type:" type
                                      " deprecated:" deprecated
                                      " related-ids  " related-ids
                                      " offset:" offset
                                      " limit:" limit))

                       {:status 200
                        :body (vec (map types/map->nsmap (concepts/find-concepts
                                                          {:id id
                                                           :preferred-label preferred-label
                                                           :type (when type (clojure.string/split type #" "))
                                                           :deprecated deprecated
                                                           :relation relation
                                                           :related-ids (when related-ids (clojure.string/split related-ids #" "))
                                                           :offset offset
                                                           :limit limit
                                                           :version version})))})}}]

    ["/accumulate-concept"
     {:summary "Accumulate data on an existing concept."
      :parameters {:query
                   (pu/build-parameter-map [:id :type :definition :preferred-label :comment :quality-level])}

      :patch {:responses {200 {:body types/ok-concept-spec}
                          409 {:body types/error-spec}
                          500 {:body types/error-spec}}
              :handler (fn [request]
                         (let [query (pu/get-query-from-request request)
                               user-id (pu/get-user-id-from-request request)]
                           (log/info "PATCH /accumulate-concept")
                           (let [result (concepts/accumulate-concept user-id query)]
                             (if result
                               {:status 200 :body (types/map->nsmap result)}
                               {:status 409 :body (types/map->nsmap {:error "Can't update concept since it is in conflict with existing concept."})}))))}}]

    ["/relation"
     {:summary "Assert a new relation."
      :parameters {:query (concepts/assert-relation-query-params)}
      :post {:responses {200 {:body types/msg-spec}
                         409 {:body types/error-spec}
                         500 {:body types/error-spec}}
             :handler concepts/assert-relation-handler}}]

    ["/delete-relation"
     {:summary "Retract a relation."
      :parameters {:query (concepts/delete-relation-query-params)}
      :delete {:responses {200 {:body types/msg-spec}
                           409 {:body types/error-spec}
                           500 {:body types/error-spec}}
               :handler concepts/delete-relation-handler}}]

    ["/graph"
     {:summary "Fetch nodes and edges from the Taxonomies. Only one depth is returned at the time."
      :parameters {:query {:edge-relation-type (taxonomy/par string? "Edge relation type")
                           :source-concept-type (taxonomy/par string? "Source nodes concept type")
                           :target-concept-type (taxonomy/par string? "Target nodes concept type")
                           (ds/opt :offset) (taxonomy/par int? "Return list offset (from 0)")
                           (ds/opt :limit) (taxonomy/par int? "Return list limit")
                           (ds/opt :version) taxonomy/version-param
                           (ds/opt :include-deprecated) (taxonomy/par boolean? "Include deprecated concepts")}}
      :get {:responses {200 {:body taxonomy/graph-spec}
                        401 {:body types/unauthorized-spec}
                        500 {:body types/error-spec}}
            :handler (fn [{{{:keys [edge-relation-type source-concept-type target-concept-type offset limit version include-deprecated]
                             :or {version :next}} :query} :parameters}]
                       {:status 200
                        :body (graph/fetch-graph edge-relation-type source-concept-type target-concept-type
                                                 include-deprecated offset limit version)})}}]

    ["/replace-concept"
     {:summary "Replace old concept with a new concept."
      :parameters {:query {:old-concept-id (taxonomy/par string? "Old concept ID"),
                           :new-concept-id (taxonomy/par string? "New concept ID")
                           (ds/opt :comment) (taxonomy/par string? "Daynote comment for this action")}}
      :post {:responses {200 {:body types/ok-spec}
                         404 {:body types/error-spec}
                         500 {:body types/error-spec}}
             :handler (fn [{{{:keys [old-concept-id new-concept-id comment]} :query} :parameters :as request}]
                        (let [user-id (pu/get-user-id-from-request request)]
                          (log/info "POST /replace-concept" old-concept-id new-concept-id)
                          (if (core/replace-deprecated-concept user-id old-concept-id new-concept-id comment)
                            {:status 200 :body (types/map->nsmap {:message "ok"})}
                            {:status 404 :body (types/map->nsmap {:error "not found"})})))}}]

    ["/versions"
     {:summary "Creates a new version tag in the database."
      :parameters {:query {:new-version-id (taxonomy/par int? "New version ID")
                           (ds/opt :new-version-timestamp) (taxonomy/par inst? "New version timestamp (default: now)")}}
      :post {:responses {200 {:body types/ok-spec}
                         406 {:body types/error-spec}
                         500 {:body types/error-spec}}
             :handler (fn [{{{:keys [new-version-id new-version-timestamp]} :query} :parameters}]
                        (log/info (str "POST /versions" new-version-id new-version-timestamp))
                        (case (v/create-new-version new-version-id new-version-timestamp)
                          ::v/incorrect-new-version
                          {:status 406
                           :body (types/map->nsmap {:error (str new-version-id " is not the next valid version id!")})}

                          ::v/new-version-before-release
                          {:status 406
                           :body (types/map->nsmap {:error (str new-version-timestamp " is before the latest release!")})}

                          (let [clients (webhooks/get-client-list-from-store!)]
                            (webhooks/notify-all! clients new-version-id)
                            {:status 200
                             :body (types/map->nsmap {:message "A new version of the Taxonomy was created."})})))}}]

    ["/concept/automatic-daynotes/"
     {:get {:summary "Fetches concept day notes (optionally restricted to a single concept id)"
            :parameters {:query {(ds/opt :id) (taxonomy/par string? "ID of concept")
                                 (ds/opt :from-timestamp) (taxonomy/par inst? "Show daynotes since this point in time (inclusive)")
                                 (ds/opt :to-timestamp) (taxonomy/par inst? "Show daynotes until this point in time (exclusive)")}}
            :responses {200 {:body [any?]}
                        404 {:body types/error-spec}
                        500 {:body types/error-spec}}
            :handler (fn [{{{:keys [id from-timestamp to-timestamp]} :query} :parameters}]
                       (log/info (str "GET /concepts/automatic-daynotes/" id from-timestamp to-timestamp))
                       {:status 200
                        :body (daynotes/get-for-concept id from-timestamp to-timestamp)})}
      :post {:summary "Create a concept daynote"
             :parameters {:query {:id (taxonomy/par string? "ID of concept")
                                  :comment (taxonomy/par string? "Comment")}}
             :responses {200 {:body types/ok-spec}
                         500 {:body types/error-spec}}
             :handler (fn [{{{:keys [id comment]} :query} :parameters :as request}]
                        (log/info "POST /concepts/automatic-daynotes/")
                        (daynotes/create-for-concept id (pu/get-user-id-from-request request) comment)
                        {:status 200
                         :body (types/map->nsmap {:message "ok"})})}}]

    ["/relation/automatic-daynotes/"
     {:get {:summary "Fetches relation day notes (optionally restricted to relations of a particular concept id)"
            :parameters {:query {(ds/opt :id) (taxonomy/par string? "ID of concept")
                                 (ds/opt :from-timestamp) (taxonomy/par inst? "Show daynotes since this date (inclusive)")
                                 (ds/opt :to-timestamp) (taxonomy/par inst? "Show daynotes until this date (exclusive)")}}
            :responses {200 {:body [any?]}
                        404 {:body types/error-spec}
                        500 {:body types/error-spec}}
            :handler (fn [{{{:keys [id from-timestamp to-timestamp]} :query} :parameters}]
                       (log/info (str "GET /relations/automatic-daynotes/" id from-timestamp to-timestamp))
                       {:status 200
                        :body (daynotes/get-for-relation id from-timestamp to-timestamp)})}
      :post {:summary "Create a relation daynote"
             :parameters {:query {:concept-1 (taxonomy/par string? "ID of source concept")
                                  :concept-2 (taxonomy/par string? "ID of target concept")
                                  :relation-type (taxonomy/par #{"broader" "related" "substitutability"} "Relation type")
                                  :comment (taxonomy/par string? "Comment")}}
             :responses {200 {:body types/ok-spec}
                         404 {:body types/error-spec}
                         500 {:body types/error-spec}}
             :handler (fn [{{{:keys [concept-1 concept-2 relation-type comment]} :query} :parameters :as request}]
                        (log/info "POST /relations/automatic/daynotes")
                        (if (daynotes/create-for-relation (pu/get-user-id-from-request request)
                                                          comment
                                                          concept-1
                                                          concept-2
                                                          relation-type)
                          {:status 200
                           :body (types/map->nsmap {:message "ok"})}
                          {:status 404
                           :body (types/map->nsmap {:error "relation not found"})}))}}]]

   ["/webhooks"
    {:swagger {:tags ["Webhooks"]}
     :middleware [cors/cors auth authorized-private?]}

    ["/register"
     {:summary ""
      :parameters {:query {:api-key (taxonomy/par string? "A unique API-key used as identifier.")
                           :callback-url (taxonomy/par string? "A URL to a webhook callback listener.")}}
      :post {:responses {200 {:body types/ok-spec}
                         406 {:body types/error-spec}
                         500 {:body types/error-spec}}
             :handler (fn [{{{:keys [api-key callback-url]} :query} :parameters}]
                        (log/info (str "POST /webhooks/register " api-key " " callback-url))
                        (let [result (store/store-update api-key callback-url)]
                          (if result
                            {:status 200 :body (types/map->nsmap
                                                {:message (format "The webhook callback was registered.")})}
                            {:status 406 :body (types/map->nsmap {:error (str "Could not create webhook callback with values " api-key " " callback-url)})})))}}]]

   ["/legacy"
    {:swagger {:tags ["Legacy"]}
     :middleware [cors/cors auth]}

    ["/convert-matching-component-id-to-new-id"
     {:summary "This endpoint is only for legacy applications wanting to convert between legacy ids from the old matching component to new Taxonomy ids. The old matching component used SSYK codes, municipality codes and region codes as ids so this endpoint implements the same bad behaviour."
      :parameters {:query {:matching-component-id (taxonomy/par string? "An id form the old matching component.")
                           :type legacy/concepts-types}}
      :get {:responses legacy/responses
            :handler legacy/convert-matching-component-id-to-new-id-handler}}]

    ["/convert-old-id-to-new-id"
     {:summary "This endpoint is only for legacy applications wanting to convert between legacy ids to new Taxonomy ids."
      :parameters {:query {:legacy-id (taxonomy/par string? "A legacy id.")
                           :type legacy/concepts-types}}
      :get {:responses legacy/responses
            :handler legacy/convert-old-id-to-new-id-handler}}]

    ["/convert-new-id-to-old-id"
     {:summary "This endpoint is only for legacy applications wanting to convert between new ids to ikd Taxonomy ids. "
      :parameters {:query {:id (taxonomy/par string? "A taxonomy id")}}

      :get {:responses legacy/responses
            :handler legacy/convert-new-id-to-old-id-handler}}]

    ["/convert-ssyk-2012-code-to-new-id"
     {:summary "This endpoint is only for legacy applications wanting to convert between SSYK 2012 code to new Taxonomy ids. SSYK 2012 is also known as occupation group / yrkesgrupp in older applications."
      :parameters {:query {:code (taxonomy/par string? "The SSYK 2012 code.")}}
      :get {:responses legacy/responses
            :handler legacy/convert-ssyk-2012-code-to-new-id-handler}}]

    ["/convert-municipality-code-to-new-id"
     {:summary "This endpoint is only for legacy applications wanting to convert between municipality code to new Taxonomy ids. "
      :parameters {:query {:code (taxonomy/par string? "The municipality code.")}}
      :get {:responses legacy/responses
            :handler legacy/convert-municipality-code-to-new-id-handler}}]

    ["/convert-swedish-region-code-to-new-id"
     {:summary "This endpoint is only for legacy applications wanting to convert between Swedish län code to new Taxonomy ids. "
      :parameters {:query {:code (taxonomy/par string? "The region code.")}}

      :get {:responses legacy/responses
            :handler legacy/convert-swedish-region-code-to-new-id}}]

    ["/get-occupation-name-with-relations"
     {:summary "This endpoint is only for legacy applications wanting a translation dump with ids for occupation names and their relations. "
      :get {:responses {200 {:body types/legacy-mapping-concepts}
                        401 {:body types/unauthorized-spec}
                        500 {:body types/error-spec}}
            :handler (fn [{{{:keys [_]} :query} :parameters}]
                       {:status 200
                        :body (map types/map->nsmap (legacy/get-occupation-name-with-relations))})}}]]])
