(ns jobtech-taxonomy-api.routes.swagger3
  (:require [reitit.coercion :as coercion]
            [muuntaja.core :as m]
            [spec-tools.swagger.core :as swagger]))

(defn wrap-coercion [coercion]
  (reify coercion/Coercion
    (-get-name [_] (coercion/-get-name coercion))
    (-get-options [_] (coercion/-get-options coercion))
    (-get-apidocs [this specification {:keys [parameters responses muuntaja]}]
      (case specification
        :swagger
        (cond-> {}
          parameters
          (assoc :parameters
                 (for [[in spec] parameters
                       ;; body is no longer specified in parameters in openapi 3.0, instead
                       ;; it is a separate "requestBody" key in the endpoint.
                       ;; See https://swagger.io/docs/specification/describing-request-body/
                       ;; if you need to add support for body in swagger.json
                       :when (not= in :body)
                       :let [schema (-> this
                                        (coercion/-compile-model spec nil)
                                        (swagger/transform {:in in :type :parameter}))
                             required (set (:required schema))]
                       [prop-name prop-schema] (:properties schema)]
                   {:in in
                    :name prop-name
                    :required (contains? required prop-name)
                    :description (:description prop-schema "")
                    :deprecated (:deprecated prop-schema false)
                    :schema (dissoc prop-schema :description :deprecated)}))
          responses
          (assoc :responses
                 (into
                  (empty responses)
                  (for [[status response] responses]
                    [status
                     (if-let [spec (:body response)]
                       (let [swagger-schema (swagger/transform (coercion/-compile-model this spec nil) {:type :schema})]
                         (reduce #(assoc-in %1 [:content %2 :schema] swagger-schema) (dissoc response :body) (m/encodes muuntaja)))
                       response)]))))
        (throw
         (ex-info
          (str "Can't produce Spec apidocs for " specification)
          {:specification specification, :coercion :spec}))))
    (-compile-model [_ model name]
      (coercion/-compile-model coercion model name))
    (-open-model [_ spec]
      (coercion/-open-model coercion spec))
    (-encode-error [_ error]
      (coercion/-encode-error coercion error))
    (-request-coercer [_ type spec]
      (coercion/-request-coercer coercion type spec))
    (-response-coercer [_ spec]
      (coercion/-response-coercer coercion spec))))